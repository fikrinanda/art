<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddIjazahColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('pekerja', function (Blueprint $table) {
            $table->string('harga', 10)->nullable();
            $table->string('sd', 250)->nullable();
            $table->string('smp', 250)->nullable();
            $table->string('sma', 250)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('pekerja', function (Blueprint $table) {
            $table->dropColumn('harga');
            $table->dropColumn('sd');
            $table->dropColumn('smp');
            $table->dropColumn('sma');
        });
    }
}
