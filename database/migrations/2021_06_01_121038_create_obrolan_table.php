<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateObrolanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('obrolan', function (Blueprint $table) {
            $table->string('pengirim', 10);
            $table->string('penerima', 10);
            $table->string('hash', 32);
            $table->string('pesan', 100);
            $table->timestamps();
            $table->foreign('penerima')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('pengirim')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('obrolan', function (Blueprint $table) {
            $table->dropForeign(['pengirim']);
            $table->dropForeign(['penerima']);
        });
        Schema::dropIfExists('obrolan');
    }
}
