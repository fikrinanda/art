<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddEmail extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('pekerja', function (Blueprint $table) {
            $table->string('email', 50)->nullable()->after('kota');
            $table->string('no_hp', 15)->nullable()->after('email');
        });

        Schema::table('pelanggan', function (Blueprint $table) {
            $table->string('email', 50)->nullable()->after('kota');
            $table->string('no_hp', 15)->nullable()->after('email');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('pekerja', function (Blueprint $table) {
            $table->dropColumn('email');
            $table->dropColumn('no_hp');
        });

        Schema::table('pelanggan', function (Blueprint $table) {
            $table->dropColumn('email');
            $table->dropColumn('no_hp');
        });
    }
}
