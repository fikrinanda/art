<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddKota extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('pekerja', function (Blueprint $table) {
            $table->string('kota', 25)->nullable()->after('alamat');
        });
        Schema::table('pelanggan', function (Blueprint $table) {
            $table->string('kota', 25)->nullable()->after('alamat');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('pekerja', function (Blueprint $table) {
            $table->dropColumn('kota');
        });
        Schema::table('pelanggan', function (Blueprint $table) {
            $table->dropColumn('kota');
        });
    }
}
