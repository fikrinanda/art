<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->string('id', 10)->primary();
            $table->string('username', 20);
            $table->string('password', 100);
            $table->string('level', 10);
            $table->timestamps();
        });

        Schema::create('pekerja', function (Blueprint $table) {
            $table->string('id', 10)->primary();
            $table->string('user_id', 10);
            $table->string('nama', 50)->nullable();
            $table->string('jenis_kelamin', 15)->nullable();
            $table->date('tanggal_lahir');
            $table->string('umur', 5)->nullable();
            $table->string('alamat', 250)->nullable();
            $table->string('jenis', 20);
            $table->string('foto', 250);
            $table->string('dokumen', 250);
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });

        Schema::create('pelanggan', function (Blueprint $table) {
            $table->string('id', 10)->primary();
            $table->string('user_id', 10);
            $table->string('nama', 50)->nullable();
            $table->string('jenis_kelamin', 15)->nullable();
            $table->date('tanggal_lahir');
            $table->string('alamat', 250)->nullable();
            $table->string('foto', 250);
            $table->string('dokumen', 250);
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });

        Schema::create('register', function (Blueprint $table) {
            $table->string('id', 10)->primary();
            $table->string('username', 20);
            $table->string('password', 100);
            $table->date('tanggal_lahir');
            $table->string('jenis', 20)->nullable();
            $table->string('level', 10);
            $table->string('foto', 250);
            $table->string('dokumen', 250);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('pekerja', function (Blueprint $table) {
            $table->dropForeign(['user_id']);
            $table->dropColumn('user_id');
        });

        Schema::table('pelanggan', function (Blueprint $table) {
            $table->dropForeign(['user_id']);
            $table->dropColumn('user_id');
        });

        Schema::dropIfExists('register');
        Schema::dropIfExists('pelanggan');
        Schema::dropIfExists('pekerja');
        Schema::dropIfExists('users');
    }
}
