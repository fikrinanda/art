<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddDibaca extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('pesan', function (Blueprint $table) {
            $table->string('is_pelanggan', 10)->nullable()->after('status');
            $table->string('is_pekerja', 10)->nullable()->after('is_pelanggan');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('pesan', function (Blueprint $table) {
            $table->dropColumn('is_pelanggan');
            $table->dropColumn('is_pekerja');
        });
    }
}
