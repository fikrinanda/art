<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddPendidikan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('pekerja', function (Blueprint $table) {
            $table->string('ket_sd', 250)->nullable()->after('sd');
            $table->string('ket_smp', 250)->nullable()->after('sd');
            $table->string('ket_sma', 250)->nullable()->after('sd');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('pekerja', function (Blueprint $table) {
            $table->dropColumn('ket_sd');
            $table->dropColumn('ket_smp');
            $table->dropColumn('ket_sma');
        });
    }
}
