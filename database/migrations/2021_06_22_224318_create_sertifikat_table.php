<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSertifikatTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sertifikat', function (Blueprint $table) {
            $table->string('pekerja_id', 10);
            $table->string('sertifikat', 250);
            $table->string('hash', 32);
            $table->timestamps();

            $table->foreign('pekerja_id')->references('id')->on('pekerja')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sertifikat', function (Blueprint $table) {
            $table->dropForeign(['pekerja_id']);
            $table->dropColumn('pekerja_id');
        });
        Schema::dropIfExists('sertifikat');
    }
}
