<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateValidasiTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('validasi', function (Blueprint $table) {
            $table->id();
            $table->string('pekerja_id', 10);
            $table->string('nama', 50)->nullable();
            $table->string('jenis_kelamin', 15)->nullable();
            $table->date('tanggal_lahir');
            $table->string('umur', 5)->nullable();
            $table->string('alamat', 250)->nullable();
            $table->string('kota', 25)->nullable();
            $table->string('email', 50)->nullable();
            $table->string('no_hp', 15)->nullable();
            $table->string('jenis', 20);
            $table->string('harga', 10)->nullable();
            $table->string('foto', 250);
            $table->string('dokumen', 250);
            $table->string('sd', 250)->nullable();
            $table->string('smp', 250)->nullable();
            $table->string('sma', 250)->nullable();
            $table->string('ket_sd', 250)->nullable();
            $table->string('ket_smp', 250)->nullable();
            $table->string('ket_sma', 250)->nullable();
            $table->string('status', 15);
            $table->timestamps();

            $table->foreign('pekerja_id')->references('id')->on('pekerja')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('validasi', function (Blueprint $table) {
            $table->dropForeign(['pekerja_id']);
        });
        Schema::dropIfExists('validasi');
    }
}
