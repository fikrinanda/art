<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePesanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('pekerja', function (Blueprint $table) {
            $table->string('status', 15)->after('jenis')->nullable();
        });

        Schema::create('pesan', function (Blueprint $table) {
            $table->string('id', 10)->primary();
            $table->string('pelanggan_id', 10);
            $table->string('pekerja_id', 10);
            $table->string('status', 10);
            $table->timestamps();

            $table->foreign('pelanggan_id')->references('id')->on('pelanggan')->onDelete('cascade');
            $table->foreign('pekerja_id')->references('id')->on('pekerja')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('pekerja', function (Blueprint $table) {
            $table->dropColumn('status');
        });
        Schema::table('pesan', function (Blueprint $table) {
            $table->dropForeign(['pelanggan_id']);
            $table->dropColumn('pelanggan_id');
            $table->dropForeign(['pekerja_id']);
            $table->dropColumn('pekerja_id');
        });
        Schema::dropIfExists('pesan');
    }
}
