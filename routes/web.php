<?php

use App\Http\Controllers\Controller;
use App\Http\Livewire\Admin\Index as AdminIndex;
use App\Http\Livewire\Admin\Data as AdminData;
use App\Http\Livewire\Admin\Tambah as AdminTambah;
use App\Http\Livewire\Admin\Ubah as AdminUbah;
use App\Http\Livewire\Login;
use App\Http\Livewire\Login2;
use App\Http\Livewire\Profil;
use App\Http\Livewire\Ubah;
use App\Http\Livewire\Password;
use App\Http\Livewire\Logout;
use App\Http\Livewire\Register\Pekerja;
use App\Http\Livewire\Register\Pelanggan;
use App\Http\Livewire\Register\Data;
use App\Http\Livewire\Register\Data2;
use App\Http\Livewire\Registrasi\Lihat;
use App\Http\Livewire\Registrasi\Lihat2;
use App\Http\Livewire\Pekerja\Index as PekerjaIndex;
use App\Http\Livewire\Pekerja\Data as PekerjaData;
use App\Http\Livewire\Pekerja\Tambah as PekerjaTambah;
use App\Http\Livewire\Pekerja\Lihat as PekerjaLihat;
use App\Http\Livewire\Pekerja\Ubah as PekerjaUbah;
use App\Http\Livewire\Pekerja\Show as PekerjaShow;
use App\Http\Livewire\Pekerja\Detail as PekerjaDetail;
use App\Http\Livewire\Pekerja\Konfirmasi as PekerjaKonfirmas;
use App\Http\Livewire\Pelanggan\Index as PelangganIndex;
use App\Http\Livewire\Pelanggan\Data as PelangganData;
use App\Http\Livewire\Pelanggan\Detail as PelangganDetail;
use App\Http\Livewire\Pelanggan\Tambah as PelangganTambah;
use App\Http\Livewire\Pelanggan\Lihat as PelangganLihat;
use App\Http\Livewire\Pelanggan\Ubah as PelangganUbah;
use App\Http\Livewire\Pelanggan\Pesan as PelangganPesan;
use App\Http\Livewire\Riwayat\Data as RiwayatData;
use App\Http\Livewire\Validasi\Data as ValidasiData;
use App\Http\Livewire\Validasi\Lihat as ValidasiLihat;
use App\Http\Livewire\Rating\Tambah as RatingTambah;
use App\Http\Livewire\Rating\Ubah as RatingUbah;
use App\Models\Obrolan;
use App\Models\User;
use Illuminate\Support\Facades\Route;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/rating', function () {
    return view('welcome');
});

Route::get('/coba', function () {
    $last = Obrolan::where(function ($q) {
        $q->where('pengirim', 'US0004');
        $q->where('penerima', 'US0005');
    })->orWhere(function ($q) {
        $q->where('penerima', 'US0004');
        $q->where('pengirim', 'US0005');
    })->latest()->first();

    $user = User::where('level', 'Pelaggan')->get();
});

Route::middleware('guest')->group(function () {
    Route::get('/', Login::class)->name('login');
    Route::get('/login', Login2::class);
    Route::get('/daftar/pekerja', Pekerja::class);
    Route::get('/daftar/pelanggan', Pelanggan::class);
});
Route::post('/logout', Controller::class)->middleware('auth');

Route::middleware('auth', 'cekAdmin')->group(function () {
    Route::get('/admin', AdminIndex::class);
    Route::get('/admin/data', AdminData::class);
    Route::get('/admin/tambah', AdminTambah::class);
    Route::get('/admin/ubah/{username}', AdminUbah::class);
    Route::get('/pekerja/data', PekerjaData::class);
    Route::get('/pekerja/tambah', PekerjaTambah::class);
    Route::get('/pekerja/lihat/{username}', PekerjaLihat::class);
    Route::get('/pekerja/ubah/{username}', PekerjaUbah::class);
    Route::get('/pelanggan/data', PelangganData::class);
    Route::get('/pelanggan/tambah', PelangganTambah::class);
    Route::get('/pelanggan/lihat/{username}', PelangganLihat::class);
    Route::get('/pelanggan/ubah/{username}', PelangganUbah::class);
    Route::get('/registrasi/pekerja', Data::class);
    Route::get('/registrasi/pekerja/{username}', Lihat::class);
    Route::get('/registrasi/pelanggan', Data2::class);
    Route::get('/registrasi/pelanggan/{username}', Lihat2::class);
    Route::get('/pekerja/validasi', ValidasiData::class);
    Route::get('/pekerja/validasi/lihat/{validasi_id}', ValidasiLihat::class);
});

Route::middleware('auth', 'cekPekerja')->group(function () {
    Route::get('/pekerja', PekerjaIndex::class);
    Route::get('/pesan/konfirmasi/{id}', PekerjaKonfirmas::class);
});

Route::middleware('auth', 'cekPelanggan')->group(function () {
    Route::get('/pelanggan', PelangganIndex::class);
    Route::get('/pekerja/show', PekerjaShow::class);
    Route::get('/pesan/{username}', PelangganPesan::class);
});

Route::get('/pekerja/detail/{username}', PekerjaDetail::class);
Route::get('/pelanggan/detail/{username}', PelangganDetail::class);
Route::get('/riwayat/data', RiwayatData::class);
Route::get('/profil', Profil::class)->middleware('auth');
Route::get('/profil/ubah', Ubah::class)->middleware('auth');
Route::get('/rating/tambah', RatingTambah::class)->middleware('auth');
Route::get('/rating/ubah', RatingUbah::class)->middleware('auth');
Route::get('/password/ubah', Password::class)->middleware('auth');
Route::get('/log', Logout::class)->middleware('auth');
