<?php

namespace App\Http\Livewire\Pekerja;

use App\Models\Pekerja;
use Livewire\Component;

class Show extends Component
{
    public $kategori = 'Asisten Rumah Tangga';
    public $search = '';
    public $kota = '';
    public $urut = 'asc';

    public function kategori($nama)
    {
        $this->kategori = $nama;
    }


    public function render()
    {
        $pekerja = Pekerja::where('jenis', 'like', '%' . $this->kategori . '%')->where('nama', 'like', '%' . $this->search . '%')->where('status', 'Aktif')->where('nama', '!=', null)->where('kota', 'like', '%' . $this->kota . '%')->orderBy('harga', $this->urut)->get();
        $kt = Pekerja::select('kota')->groupBy('kota')->get();
        return view('livewire.pekerja.show', compact(['pekerja', 'kt']))->extends('layouts.pelanggan', ['title' => 'Pekerja'])->section('content');
    }
}
