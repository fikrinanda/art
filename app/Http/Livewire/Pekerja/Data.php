<?php

namespace App\Http\Livewire\Pekerja;

use App\Models\Pekerja;
use App\Models\User;
use Illuminate\Support\Facades\Storage;
use Livewire\Component;
use Livewire\WithPagination;

class Data extends Component
{
    use WithPagination;

    public $search = '';
    public $perPage = 5;
    protected $paginationTheme = 'bootstrap';
    protected $listeners = ['yakin' => 'hancur', 'batal'];
    public $sortBy = 'pekerja.id';
    public $sortDirection = 'asc';
    public $by = "semua";
    public $status;
    public $s;
    protected $rules = [
        'status.*' => 'required',
    ];

    public function updatingSearch()
    {
        $this->resetPage();
    }

    public function mount()
    {
        $this->status = Pekerja::get();

        foreach ($this->status as $key => $value) {
            $this->s[] = $value->status;
        }
    }

    public function updatedS()
    {
        foreach ($this->status as $key => $value) {
            Pekerja::where('id', $value->id)->update([
                'status' => $this->s[$key]
            ]);
        }
    }

    public function hapus($id)
    {
        $user = User::find($id);
        $this->showConfirmation($user->id, $user->pekerja->nama);
    }

    public function hancur($id)
    {
        $user = User::find($id);
        $nama = $user->pekerja->nama;
        $pekerja = Pekerja::where('user_id', $id)->first();
        Storage::disk('public')->delete($pekerja->foto);
        Storage::disk('public')->delete($pekerja->dokumen);
        $user->delete();
        $this->showModal($nama);
    }


    public function batal()
    {
        // dd('batal');
    }

    public function showModal($nama)
    {
        $this->emit('swal:modal', [
            'icon'  => 'success',
            'title' => 'Berhasil!!!',
            'text'  => "Data Pekerja $nama berhasil dihapus",
        ]);
    }

    public function showAlert()
    {
        $this->emit('swal:alert', [
            'type'    => 'success',
            'title'   => 'This is a success alert!!',
            'timeout' => 5000
        ]);
    }

    public function showConfirmation($id, $nama)
    {
        $this->emit("swal:confirm", [
            'icon'        => 'warning',
            'title'       => "Yakin menghapus Pekerja $nama?",
            'text'        => "Setalah dihapus, anda tidak dapat mengembalikan data ini!",
            'confirmText' => 'Ya, hapus!',
            'method'      => 'appointments:delete',
            'params'      => $id, // optional, send params to success confirmation
            'callback'    => '', // optional, fire event if no confirmed
        ]);
    }

    public function sortBy($field)
    {
        if ($this->sortDirection == 'asc') {
            $this->sortDirection = 'desc';
        } else {
            $this->sortDirection = 'asc';
        }

        return $this->sortBy = $field;
    }

    public function render()
    {
        $pekerja = Pekerja::selectRaw('pekerja.*')->join('users', 'users.id', '=', 'pekerja.user_id')->where('nama', 'like', '%' . $this->search . '%')->orWhere('username', 'like', '%' . $this->search . '%')->orderBy($this->sortBy, $this->sortDirection)->paginate($this->perPage);
        return view('livewire.pekerja.data', compact(['pekerja']))->extends('layouts.admin', ['title' => 'Data Pekerja'])->section('content');
    }
}
