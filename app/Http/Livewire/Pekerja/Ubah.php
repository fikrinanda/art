<?php

namespace App\Http\Livewire\Pekerja;

use App\Models\Pekerja;
use App\Models\Sertifikat;
use App\Models\User;
use App\Models\Validasi;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Livewire\Component;
use Livewire\WithFileUploads;

class Ubah extends Component
{
    use WithFileUploads;
    public $nama;
    public $jenis_kelamin;
    public $tanggal_lahir;
    public $alamat;
    public $jenis;
    public $username;
    public $foto;
    public $foto2;
    public $ktp;
    public $ktp2;
    public $sd;
    public $sd2;
    public $smp;
    public $smp2;
    public $sma;
    public $sma2;
    public $tgl;
    public $harga;
    public $sertifikat = [];
    public $kota;
    public $ket_sd;
    public $tahun_sd1;
    public $tahun_sd2;
    public $ket_smp;
    public $tahun_smp1;
    public $tahun_smp2;
    public $ket_sma;
    public $tahun_sma1;
    public $tahun_sma2;
    public $email;
    public $no_hp;
    public $ser;
    public $i;
    public $i2;
    public $z;
    public $s;
    public $ds;
    public $shin;
    public $ryujin;
    protected $listeners = ['yakin' => 'hancur', 'batal', 'berhasil', 'berhasil2'];

    protected $rules = [
        'ser.*.keterangan' => 'required',
        'ser.*.sertifikat' => 'required'
    ];

    public function mount($username)
    {
        $this->tgl = Carbon::now()->format('Y-m-d');

        $user = User::where('username', $username)->first();
        if ($user) {
            $this->sertifikat = [
                []
            ];
            $this->i = $user->pekerja->id;
            $this->i2 = $user->id;
            $this->ser = Sertifikat::where('pekerja_id', $this->i)->where('status', 'Konfirmasi')->get();
            foreach ($this->ser as $key => $value) {
                $this->s[] = [];
                $this->z[$key]['keterangan'] = $value->keterangan;
                $this->z[$key]['sertifikat'] = $value->sertifikat;
                $this->ds[$key]['sertifikat'] = '';
            }
            $this->shin = count($this->ser);
            $this->username = $user->username;
            $this->nama = $user->pekerja->nama;
            $this->jenis_kelamin = $user->pekerja->jenis_kelamin;
            $this->tanggal_lahir = Carbon::parse($user->pekerja->tanggal_lahir)->format('Y-m-d');
            $this->umur = $user->pekerja->umur;
            $this->alamat = $user->pekerja->alamat;
            $this->kota = $user->pekerja->kota;
            $this->foto = $user->pekerja->foto;
            $this->ktp = $user->pekerja->dokumen;
            $this->sd = $user->pekerja->sd;
            $this->smp = $user->pekerja->smp;
            $this->sma = $user->pekerja->sma;
            $this->jenis = $user->pekerja->jenis;
            $this->harga = $user->pekerja->harga;
            $this->email = $user->pekerja->email;
            $this->no_hp = $user->pekerja->no_hp;
            if ($user->pekerja->ket_sd != null) {
                $sd = explode("-", $user->pekerja->ket_sd);
                $sd2 = explode("/", $sd[1]);
                $this->ket_sd = $sd[0];
                $this->tahun_sd1 = $sd2[0];
                $this->tahun_sd2 = $sd2[1];
            }
            if ($user->pekerja->ket_smp != null) {
                $smp = explode("-", $user->pekerja->ket_smp);
                $smp2 = explode("/", $smp[1]);
                $this->ket_smp = $smp[0];
                $this->tahun_smp1 = $smp2[0];
                $this->tahun_smp2 = $smp2[1];
            }
            if ($user->pekerja->ket_sma != null) {
                $sma = explode("-", $user->pekerja->ket_sma);
                $sma2 = explode("/", $sma[1]);
                $this->ket_sma = $sma[0];
                $this->tahun_sma1 = $sma2[0];
                $this->tahun_sma2 = $sma2[1];
            }
        } else {
            abort('404');
        }
    }

    public function del($index)
    {
        $this->ds[$index]['sertifikat'] = 'd-none';
        $this->z[$index]['sertifikat'] = null;
        $this->z[$index]['keterangan'] = null;
    }

    public function bat($index)
    {
        $this->ds[$index]['sertifikat'] = '';
        $this->z[$index]['sertifikat'] = $this->ser[$index]['sertifikat'];
    }


    public function add()
    {
        $this->sertifikat[] = [];
    }

    public function remove($index)
    {
        if (count($this->sertifikat) > 1) {
            unset($this->sertifikat[$index]);
            $this->sertifikat = array_values($this->sertifikat);
        }
    }

    public function updated($field)
    {
        $this->validateOnly($field, [
            'username' => 'required|min:8|max:24|unique:users,username,' . $this->i2,
            'tanggal_lahir' => 'before:today|after:01/01/1940|date|before:' . Carbon::now()->subYears(17)->format('d M Y') . '',
            // 'foto2' => 'image|max:5000',
            // 'ktp2' => 'image|max:5000',
            // 'sd2' => 'image|max:5000',
            // 'smp2' => 'image|max:5000',
            // 'sma2' => 'image|max:5000',
            'nama' => 'required|regex:/^([^0-9]*)$/|min:3',
            'jenis_kelamin' => 'required',
            'alamat' => 'required|min:5',
            'kota' => 'required|',
            'jenis' => 'required',
            'z.*.keterangan' => 'required',
            'z.*.sertifikat' => 'required',
            'harga' => 'required|numeric|min:100000',
        ]);
    }

    public function ubah()
    {
        $this->validate([
            'username' => 'required|min:8|max:24|unique:users,username,' . $this->i2,
            'tanggal_lahir' => 'before:today|after:01/01/1940|date|before:' . Carbon::now()->subYears(17)->format('d M Y') . '',
            // 'foto2' => 'image|max:5000',
            // 'ktp2' => 'image|max:5000',
            // 'sd2' => 'image|max:5000',
            // 'smp2' => 'image|max:5000',
            // 'sma2' => 'image|max:5000',
            'nama' => 'required|regex:/^([^0-9]*)$/|min:3',
            'jenis_kelamin' => 'required',
            'alamat' => 'required|min:5',
            'kota' => 'required|',
            'jenis' => 'required',
            'z.*.keterangan' => 'required',
            'z.*.sertifikat' => 'required',
            'harga' => 'required|numeric|min:100000',
        ]);

        if (count($this->sertifikat) > 1) {
            $this->validate([
                'sertifikat.*.keterangan' => 'required',
                'sertifikat.*.gambar' => 'required',
            ]);
        }

        if ($this->foto2) {
            Storage::disk('public')->delete($this->foto);
            $foto2 = $this->foto2->store('images/pekerja/foto', 'public');
        } else {
            $foto2 = $this->foto ?? null;
        }
        if ($this->ktp2) {
            Storage::disk('public')->delete($this->ktp);
            $ktp2 = $this->ktp2->store('images/pekerja/ktp', 'public');
        } else {
            $ktp2 = $this->ktp ?? null;
        }
        if ($this->sd2) {
            Storage::disk('public')->delete($this->sd);
            $sd2 = $this->sd2->store('images/pekerja/sd', 'public');
        } else {
            $sd2 = $this->sd ?? null;
        }
        if ($this->smp2) {
            Storage::disk('public')->delete($this->smp);
            $smp2 = $this->smp2->store('images/pekerja/smp', 'public');
        } else {
            $smp2 = $this->smp ?? null;
        }
        if ($this->sma2) {
            Storage::disk('public')->delete($this->sma);
            $sma2 = $this->sma2->store('images/pekerja/sma', 'public');
        } else {
            $sma2 = $this->sma ?? null;
        }

        User::where('id', $this->i2)->update([
            'username' => $this->username,
        ]);
        $umur = Carbon::parse($this->tanggal_lahir)->age;


        Pekerja::where('id', $this->i)->update([
            'nama' => $this->nama,
            'jenis_kelamin' => $this->jenis_kelamin,
            'tanggal_lahir' => $this->tanggal_lahir,
            'umur' => $umur,
            'alamat' => $this->alamat,
            'kota' => $this->kota,
            'email' => $this->email,
            'no_hp' => $this->no_hp,
            'jenis' => $this->jenis,
            'foto' => $foto2,
            'dokumen' => $ktp2,
            'sd' => $sd2,
            'smp' => $smp2,
            'sma' => $sma2,
            'harga' => $this->harga,
            'ket_sd' => $this->ket_sd . '-' . $this->tahun_sd1 . '/' . $this->tahun_sd2,
            'ket_smp' => $this->ket_smp . '-' . $this->tahun_smp1 . '/' . $this->tahun_smp2,
            'ket_sma' => $this->ket_sma . '-' . $this->tahun_sma1 . '/' . $this->tahun_sma2,
        ]);

        User::where('id', $this->i2)->update([
            'username' => $this->username,
        ]);

        if (isset($this->sertifikat[0]['keterangan']) && isset($this->sertifikat[0]['gambar'])) {
            foreach ($this->sertifikat as $s) {
                $ser = $s['gambar']->store('images/pekerja/sertifikat', 'public');
                Sertifikat::create([
                    'pekerja_id' => $this->i,
                    'keterangan' => $s['keterangan'],
                    'sertifikat' => $ser,
                    'hash' => Str::random(32),
                    'status' => 'Konfirmasi'
                ]);
            }
        }

        foreach ($this->ser as $k => $value) {
            if (isset($this->s[$k]['sertifikat'])) {
                if (isset($this->z[$k]['sertifikat'])) {
                    Storage::disk('public')->delete($this->z[$k]['sertifikat']);
                }
                $karina = $this->s[$k]['sertifikat']->store('images/soal', 'public');
            } else {
                if ($this->z[$k]['sertifikat'] == null) {
                    Storage::disk('public')->delete($this->z[$k]['sertifikat']);
                    $karina = null;
                } else {
                    $karina = $this->z[$k]['sertifikat'] ?? null;
                }
            }

            Sertifikat::where('pekerja_id', $this->i)->where('hash', $value->hash)->update([
                'keterangan' => $this->z[$k]['keterangan'],
                'sertifikat' => $karina,
            ]);
        }


        $this->showModal();
    }

    public function showModal()
    {
        $this->emit('swal:modal', [
            'icon'  => 'success',
            'title' => 'Berhasil!!!',
            'text'  => "Berhasil ubah Pekerja $this->nama",
        ]);
    }

    public function berhasil()
    {
        return redirect()->to('/pekerja/data');
    }

    public function hapus($id)
    {
        $user = Sertifikat::where('hash', $id)->first();
        $this->showConfirmation($id, $user->keterangan);
    }

    public function hancur($id)
    {
        $user = Sertifikat::where('hash', $id);
        $nama = $user->first()->keterangan;
        $this->ryujin = $id;
        $this->showModal2($nama);
    }

    public function berhasil2()
    {
        $user = Sertifikat::where('hash', $this->ryujin);
        $user->delete();
        $a = User::find($this->i2)->username;
        return redirect()->to("/pekerja/ubah/$a");
    }


    public function batal()
    {
        // dd('batal');
    }

    public function showModal2($nama)
    {
        $this->emit('swal:modal2', [
            'icon'  => 'success',
            'title' => 'Berhasil!!!',
            'text'  => "Dokumen $nama berhasil dihapus",
        ]);
    }

    public function showConfirmation($id, $nama)
    {
        $this->emit("swal:confirm", [
            'icon'        => 'warning',
            'title'       => "Yakin menghapus Dokumen $nama?",
            'text'        => "Setalah dihapus, anda tidak dapat mengembalikan data ini!",
            'confirmText' => 'Ya, hapus!',
            'method'      => 'appointments:delete',
            'params'      => $id, // optional, send params to success confirmation
            'callback'    => '', // optional, fire event if no confirmed
        ]);
    }

    public function render()
    {
        return view('livewire.pekerja.ubah')->extends('layouts.admin', ['title' => 'Ubah Pekerja'])->section('content');
    }
}
