<?php

namespace App\Http\Livewire\Pekerja;

use App\Models\Pekerja;
use App\Models\Pelanggan;
use App\Models\Pesan;
use App\Models\User;
use Livewire\Component;

class Konfirmasi extends Component
{
    public $pelanggan;
    public $pesan;
    protected $listeners = ['yakin' => 'hancur', 'batal', 'yakin2' => 'hancur2', 'batal2', 'berhasil'];

    public function mount($id)
    {
        $test = Pesan::where('id', $id)->where('status', 'Pending')->exists();
        $test2 = Pesan::where('id', $id)->where('status', 'Ajukan')->exists();
        if ($test || $test2) {
            $this->pesan = Pesan::find($id);
            $this->pelanggan = Pelanggan::find($this->pesan->pelanggan_id);
        } else {
            abort('404');
        }
    }

    public function konfirmasi($id)
    {
        $this->showConfirmation($id, $this->pelanggan->nama);
    }

    public function tolak($id)
    {
        $this->showConfirmation2($id, $this->pelanggan->nama);
    }

    public function hancur($id)
    {
        if (Pesan::find($id)->status == 'Ajukan') {
            # code...
            Pesan::where('id', $id)->update([
                'status' => 'Konfirmasi'
            ]);
            Pekerja::where('id', auth()->user()->pekerja->id)->update([
                'status' => 'Dipesan',
            ]);

            $this->showModal("Berhasil konfirmasi setuju dengan pelanggan " . $this->pelanggan->nama);
        } else {
            Pesan::where('id', $id)->update([
                'status' => 'Chat'
            ]);
            $this->showModal("Berhasil konfirmasi negosiasi dengan pelanggan " . $this->pelanggan->nama);
        }
    }

    public function hancur2($id)
    {
        if (Pesan::find($id)->status == 'Ajukan') {
            # code...
            Pesan::where('id', $id)->update([
                'status' => 'Tolak',
                'is_pelanggan' => 'Belum',
                'is_pekerja' => 'Belum',
            ]);
            Pekerja::where('id', auth()->user()->pekerja->id)->update([
                'status' => 'Aktif',
            ]);

            $this->showModal("Berhasil menolak pesanan dengan pelanggan " . $this->pelanggan->nama);
        } else {
            Pesan::where('id', $id)->update([
                'status' => 'Tolak',
                'is_pelanggan' => 'Belum',
                'is_pekerja' => 'Belum',
            ]);
            Pekerja::where('id', auth()->user()->pekerja->id)->update([
                'status' => 'Aktif',
            ]);
            $this->showModal("Berhasil menolak pesanan dengan pelanggan " . $this->pelanggan->nama);
        }
    }

    public function batal()
    {
        // dd('batal');
    }

    public function batal2()
    {
        // dd('batal');
    }

    public function showModal($nama)
    {
        $this->emit('swal:modal', [
            'icon'  => 'success',
            'title' => 'Berhasil!!!',
            'text'  => $nama,
        ]);
    }

    public function berhasil()
    {
        return redirect()->to('/pekerja');
    }

    public function showAlert()
    {
        $this->emit('swal:alert', [
            'type'    => 'success',
            'title'   => 'This is a success alert!!',
            'timeout' => 5000
        ]);
    }

    public function showConfirmation($id, $nama)
    {
        $this->emit("swal:confirm", [
            'icon'        => 'warning',
            'title'       => "Yakin konfirmasi negosiasi dengan $nama?",
            'text'        => "",
            'confirmText' => 'Ya!',
            'method'      => 'appointments:delete',
            'params'      => $id, // optional, send params to success confirmation
            'callback'    => '', // optional, fire event if no confirmed
        ]);
    }

    public function showConfirmation2($id, $nama)
    {
        $this->emit("swal:confirm2", [
            'icon'        => 'warning',
            'title'       => "Yakin tolak pesan dengan $nama?",
            'text'        => "",
            'confirmText' => 'Ya!',
            'method'      => 'appointments:delete',
            'params'      => $id, // optional, send params to success confirmation
            'callback'    => '', // optional, fire event if no confirmed
        ]);
    }

    public function render()
    {
        return view('livewire.pekerja.konfirmasi')->extends('layouts.pekerja', ['title' => 'Konfirmasi Pesan'])->section('content');
    }
}
