<?php

namespace App\Http\Livewire\Pekerja;

use App\Models\Pekerja;
use App\Models\Sertifikat;
use App\Models\User;
use Livewire\Component;

class Lihat extends Component
{
    public $pekerja;
    public $i;
    public $user;
    public $i2;
    public $pesan;
    public $ob;
    public $no = 0;
    public $temp;
    public $f;
    public $dz = "dz-chat-user";
    public $sd;
    public $smp;
    public $sma;
    protected $listeners = ['berhasil'];

    public function mount($username)
    {
        $test = User::where('username', $username)->exists();
        if ($test) {
            $user = User::where('username', $username)->first();
            $this->pekerja = Pekerja::where('user_id', $user->id)->first();
            $this->i = $user->pekerja->id;
            $this->sd = explode("-", $this->pekerja->ket_sd);
            $this->smp = explode("-", $this->pekerja->ket_smp);
            $this->sma = explode("-", $this->pekerja->ket_sma);
        } else {
            abort('404');
        }
    }

    public function pesan($id)
    {

        if (Pesan::where('pelanggan_id', auth()->user()->pelanggan->id)->count() >= 3) {
            $this->showAlert();
        } else {
            $x = Pesan::max('id');
            $y = (int) substr($x, 2, 4);
            $y++;
            $z = "PS" . sprintf("%04s", $y);

            Pesan::create([
                'id' => $z,
                'pelanggan_id' => auth()->user()->pelanggan->id,
                'pekerja_id' => $id,
                'status' => 'Pending',
                'is_pelanggan' => 'Belum',
                'is_pekerja' => 'Belum',
            ]);

            Pekerja::where('id', $id)->update([
                'status' => 'Dipesan'
            ]);

            $this->showModal();
        }
    }

    public function showModal()
    {
        $this->emit('swal:modal', [
            'icon'  => 'success',
            'title' => 'Berhasil!!!',
            'text'  => "Berhasil pesan pekerja",
        ]);
    }

    public function showAlert()
    {
        $this->emit('swal:alert', [
            'icon'    => 'error',
            'title'   => 'Maksimal pesan pekerja adalah tiga kali!!',
            'timeout' => 10000
        ]);
    }

    public function berhasil()
    {
        return redirect()->to('/pekerja/show');
    }

    public function render()
    {
        $ser = Sertifikat::where('pekerja_id', $this->i)->get();
        return view('livewire.pekerja.lihat', compact(['ser']))->extends('layouts.admin', ['title' => 'Pekerja'])->section('content');
    }
}
