<?php

namespace App\Http\Livewire\Pekerja;

use App\Models\Pekerja;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Hash;
use Livewire\Component;
use Livewire\WithFileUploads;

class Tambah extends Component
{
    use WithFileUploads;

    public $nama;
    public $jenis_kelamin;
    public $tanggal_lahir;
    public $alamat;
    public $jenis;
    public $foto;
    public $ktp;
    public $username;
    public $password;
    public $tgl;
    public $harga;
    public $sd;
    public $smp;
    public $sma;
    public $sertifikat = [];
    public $kota;
    public $ket_sd;
    public $tahun_sd1;
    public $tahun_sd2;
    public $ket_smp;
    public $tahun_smp1;
    public $tahun_smp2;
    public $ket_sma;
    public $tahun_sma1;
    public $tahun_sma2;
    public $email;
    public $no_hp;
    protected $listeners = ['berhasil'];

    public function mount()
    {
        $this->tgl = Carbon::now()->format('Y-m-d');
    }

    public function updated($field)
    {
        $this->validateOnly($field, [
            'nama' => 'required|regex:/^([^0-9]*)$/|min:3',
            'jenis_kelamin' => 'required',
            'tanggal_lahir' => 'required|before:today|after:01/01/1940|date|before:' . Carbon::now()->subYears(17)->format('d M Y') . '',
            'alamat' => 'required|min:5',
            'jenis' => 'required',
            'username' => 'required|regex:/^[\w-]*$/|unique:users,username|min:6|max:12',
            'password' => 'required|min:8',
            'foto' => 'required|image|max:5000',
            'ktp' => 'required|image|max:5000',
            'harga' => 'required|numeric',
            'sd' => 'required|image|max:5000',
            'smp' => 'image|max:5000',
            'sma' => 'image|max:5000',
        ]);
    }

    public function tambah()
    {
        $this->validate([
            'nama' => 'required|regex:/^([^0-9]*)$/|min:3',
            'jenis_kelamin' => 'required',
            'tanggal_lahir' => 'required|before:today|after:01/01/1940|date|before:' . Carbon::now()->subYears(17)->format('d M Y') . '',
            'alamat' => 'required|min:5',
            'jenis' => 'required',
            'username' => 'required|regex:/^[\w-]*$/|unique:users,username|min:6|max:12',
            'password' => 'required|min:8',
            'foto' => 'required|image|max:5000',
            'ktp' => 'required|image|max:5000',
            'harga' => 'required|numeric',
            'sd' => 'required|image|max:5000',
            'smp' => 'image|max:5000',
            'sma' => 'image|max:5000',
        ]);

        $umur = Carbon::parse($this->tanggal_lahir)->age;
        $foto = $this->foto->store('images/pekerja/foto', 'public');
        $ktp = $this->ktp->store('images/pekerja/ktp', 'public');
        if (!is_null($this->sd)) {
            $sd = $this->sd->store('images/pekerja/sd', 'public');
        } else {
            $sd = null;
        }
        if (!is_null($this->smp)) {
            $smp = $this->smp->store('images/pekerja/smp', 'public');
        } else {
            $smp = null;
        }
        if (!is_null($this->sma)) {
            $sma = $this->sma->store('images/pekerja/sma', 'public');
        } else {
            $sma = null;
        }

        $x = User::max('id');
        $y = (int) substr($x, 2, 4);
        $y++;
        $z = "US" . sprintf("%04s", $y);

        User::create([
            'id' => $z,
            'username' => $this->username,
            'password' => Hash::make($this->password),
            'level' => 'Pekerja',
        ]);

        $user = User::where('username', $this->username)->first();

        $x2 = Pekerja::max('id');
        $y2 = (int) substr($x2, 2, 4);
        $y2++;
        $z2 = "PK" . sprintf("%04s", $y2);

        Pekerja::create([
            'id' => $z2,
            'user_id' => $user->id,
            'nama' => $this->nama,
            'jenis_kelamin' => $this->jenis_kelamin,
            'tanggal_lahir' => $this->tanggal_lahir,
            'umur' => $umur,
            'alamat' => $this->alamat,
            'kota' => $this->kota,
            'email' => $this->email,
            'no_hp' => $this->no_hp,
            'jenis' => $this->jenis,
            'status' => 'Aktif',
            'foto' => $foto,
            'dokumen' => $ktp,
            'harga' => $this->harga,
            'sd' => $sd,
            'smp' => $smp,
            'sma' => $sma,
            'ket_sd' => $this->ket_sd . '-' . $this->tahun_sd1 . '/' . $this->tahun_sd2,
            'ket_smp' => $this->ket_smp . '-' . $this->tahun_smp1 . '/' . $this->tahun_smp2,
            'ket_sma' => $this->ket_sma . '-' . $this->tahun_sma1 . '/' . $this->tahun_sma2,
        ]);

        $this->showModal();
    }

    public function showModal()
    {
        $this->emit('swal:modal', [
            'icon'  => 'success',
            'title' => 'Berhasil!!!',
            'text'  => "Data Pekerja $this->nama berhasil ditambahkan",
        ]);
    }

    public function berhasil()
    {
        return redirect()->to('/pekerja/data');
    }

    public function render()
    {
        return view('livewire.pekerja.tambah')->extends('layouts.admin', ['title' => 'Tambah Pekerja'])->section('content');
    }
}
