<?php

namespace App\Http\Livewire\Pekerja;

use App\Models\Rating;
use Livewire\Component;
use Livewire\WithPagination;

class Index extends Component
{
    use WithPagination;

    public $search = '';
    public $perPage = 5;
    protected $paginationTheme = 'bootstrap';
    public $test;

    public function mount()
    {
        $this->test = Rating::where('user_id', auth()->user()->id)->exists();
    }

    public function updatingSearch()
    {
        $this->resetPage();
    }

    public function render()
    {
        $rating = Rating::selectRaw('rating.*')->join('users', 'users.id', '=', 'rating.user_id')->where('keterangan', 'like', '%' . $this->search . '%')->orWhere('username', 'like', '%' . $this->search . '%')->paginate($this->perPage);
        $count = Rating::count('*');
        $sum = Rating::sum('rating');
        $avg = $sum / $count;
        return view('livewire.pekerja.index', compact(['rating', 'avg']))->extends('layouts.pekerja', ['title' => 'Pekerja'])->section('content');
    }
}
