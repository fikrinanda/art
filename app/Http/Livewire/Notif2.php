<?php

namespace App\Http\Livewire;

use App\Models\Pesan;
use Livewire\Component;

class Notif2 extends Component
{
    public $pelanggan;

    public function mount()
    {
        $this->pelanggan = Pesan::where('pelanggan_id', auth()->user()->pelanggan->id)->get();
    }

    public function hydrate()
    {
        $this->pelanggan = Pesan::where('pelanggan_id', auth()->user()->pelanggan->id)->get();
    }

    public function halo()
    {
        Pesan::where('pelanggan_id', auth()->user()->pelanggan->id)->update([
            'is_pelanggan' => 'Sudah'
        ]);
    }

    public function render()
    {
        return view('livewire.notif2');
    }
}
