<?php

namespace App\Http\Livewire;

use App\Models\Pesan;
use Livewire\Component;

class Angka extends Component
{
    public $angka;

    public function mount()
    {
        if (Pesan::where('pekerja_id', auth()->user()->pekerja->id)->where('is_pekerja', 'Belum')->exists()) {
            $this->angka = Pesan::where('pekerja_id', auth()->user()->pekerja->id)->where('is_pekerja', 'Belum')->count();
        }
    }

    public function hydrate()
    {
        if (Pesan::where('pekerja_id', auth()->user()->pekerja->id)->where('is_pekerja', 'Belum')->exists()) {
            $this->angka = Pesan::where('pekerja_id', auth()->user()->pekerja->id)->where('is_pekerja', 'Belum')->count();
        }
    }

    public function render()
    {
        return view('livewire.angka');
    }
}
