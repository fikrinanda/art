<?php

namespace App\Http\Livewire\Rating;

use App\Models\Rating;
use Livewire\Component;

class Tambah extends Component
{
    public $rating;
    public $keterangan;
    protected $listeners = ['berhasil', 'getLatitudeForInput'];

    public function mount()
    {
        if (auth()->user()->level == 'Admin') {
            abort('404');
        }
    }

    public function tambah()
    {
        // dd($this->rating);
        Rating::create([
            'user_id' => auth()->user()->id,
            'keterangan' => $this->keterangan,
            'rating' => $this->rating
        ]);

        $this->showModal();
    }

    public function getLatitudeForInput($value)
    {
        if (!is_null($value))
            $this->rating = $value;
    }

    public function showModal()
    {
        $this->emit('swal:modal', [
            'icon'  => 'success',
            'title' => 'Berhasil!!!',
            'text'  => "Berhasil memberi rating",
        ]);
    }

    public function berhasil()
    {
        if (auth()->user()->level == 'Pelanggan') {
            return redirect()->to('/pelanggan');
        } else if (auth()->user()->level == 'Pekerja') {
            return redirect()->to('/pekerja');
        }
    }

    public function render()
    {
        if (auth()->user()->level == 'Pelanggan') {
            return view('livewire.rating.tambah')->extends('layouts.pelanggan', ['title' => 'Beri Rating'])->section('content');
        } else if (auth()->user()->level == 'Pekerja') {
            return view('livewire.rating.tambah')->extends('layouts.pekerja', ['title' => 'Beri Rating'])->section('content');
        }
    }
}
