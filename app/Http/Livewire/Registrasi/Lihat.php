<?php

namespace App\Http\Livewire\Registrasi;

use App\Models\Pekerja;
use App\Models\Register;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Livewire\Component;

class Lihat extends Component
{
    public $i;
    public $uname;
    public $password;
    public $tanggal_lahir;
    public $jenis;
    public $foto;
    public $ktp;
    protected $listeners = ['yakin' => 'hancur', 'batal', 'berhasil', 'yakin2' => 'hancur2'];

    public function mount($username)
    {
        $regis = Register::where('username', $username)->first();

        if ($regis) {
            $this->i = $regis->id;
            $this->uname = $regis->username;
            $this->password = $regis->password;
            $this->tanggal_lahir = $regis->tanggal_lahir;
            $this->jenis = $regis->jenis;
            $this->foto = $regis->foto;
            $this->ktp = $regis->dokumen;
        } else {
            abort('404');
        }
    }

    public function tolak()
    {
        $this->showConfirmation2($this->i);
    }

    public function hancur2($i)
    {
        Storage::disk('public')->delete(Register::where('id', $i)->first()->foto);
        Storage::disk('public')->delete(Register::where('id', $i)->first()->dokumen);
        Register::find($i)->delete();
        return redirect()->to('/pekerja/data');
    }

    public function konfirmasi()
    {
        $this->showConfirmation($this->i);
    }

    public function hancur($id)
    {
        $umur = Carbon::parse($this->tanggal_lahir)->age;

        $x = User::max('id');
        $y = (int) substr($x, 2, 4);
        $y++;
        $z = "US" . sprintf("%04s", $y);

        User::create([
            'id' => $z,
            'username' => $this->uname,
            'password' => $this->password,
            'level' => 'Pekerja',
        ]);

        $user = User::where('username', $this->uname)->first();

        $x2 = Pekerja::max('id');
        $y2 = (int) substr($x2, 2, 4);
        $y2++;
        $z2 = "PK" . sprintf("%04s", $y2);

        Pekerja::create([
            'id' => $z2,
            'user_id' => $user->id,
            'tanggal_lahir' => $this->tanggal_lahir,
            'umur' => $umur,
            'jenis' => $this->jenis,
            'status' => 'Aktif',
            'foto' => $this->foto,
            'dokumen' => $this->ktp,
        ]);

        Register::find($id)->delete();
        $this->showModal();
    }

    public function batal()
    {
        // dd('batal');
    }

    public function showConfirmation($id)
    {
        $this->emit("swal:confirm", [
            'icon'        => 'warning',
            'title'       => 'Yakin konfirmasi?',
            'text'        => "Mohon periksa data sebelum konfirmasi!",
            'confirmText' => 'Ya, konfirmasi!',
            'method'      => 'appointments:delete',
            'params'      => $id, // optional, send params to success confirmation
            'callback'    => '', // optional, fire event if no confirmed
        ]);
    }

    public function showConfirmation2($id)
    {
        $this->emit("swal:confirm2", [
            'icon'        => 'warning',
            'title'       => 'Yakin menolak?',
            'text'        => "Mohon periksa data sebelum menolak!",
            'confirmText' => 'Ya, tolak!',
            'method'      => 'appointments:delete',
            'params'      => $id, // optional, send params to success confirmation
            'callback'    => '', // optional, fire event if no confirmed
        ]);
    }

    public function showModal()
    {
        $this->emit('swal:modal', [
            'icon'  => 'success',
            'title' => 'Berhasil!!!',
            'text'  => "Data Pekerja $this->uname berhasil diverifikasi",
        ]);
    }

    public function berhasil()
    {
        return redirect()->to('/pekerja/data');
    }

    public function render()
    {
        return view('livewire.registrasi.lihat')->extends('layouts.admin', ['title' => 'Lihat Registrasi Pekerja'])->section('content');
    }
}
