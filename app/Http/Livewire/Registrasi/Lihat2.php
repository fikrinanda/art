<?php

namespace App\Http\Livewire\Registrasi;

use App\Models\Pelanggan;
use App\Models\Register;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Livewire\Component;

class Lihat2 extends Component
{
    public $i;
    public $uname;
    public $password;
    public $tanggal_lahir;
    public $foto;
    public $ktp;
    protected $listeners = ['yakin' => 'hancur', 'batal', 'berhasil', 'yakin2' => 'hancur2'];

    public function mount($username)
    {
        $regis = Register::where('username', $username)->first();

        if ($regis) {
            $this->i = $regis->id;
            $this->uname = $regis->username;
            $this->password = $regis->password;
            $this->tanggal_lahir = $regis->tanggal_lahir;
            $this->foto = $regis->foto;
            $this->ktp = $regis->dokumen;
        } else {
            abort('404');
        }
    }

    public function tolak()
    {
        $this->showConfirmation2($this->i);
    }

    public function hancur2($i)
    {
        Storage::disk('public')->delete(Register::where('id', $i)->first()->foto);
        Storage::disk('public')->delete(Register::where('id', $i)->first()->dokumen);
        Register::find($i)->delete();
        return redirect()->to('/pelanggan/data');
    }

    public function konfirmasi()
    {
        $this->showConfirmation($this->i);
    }

    public function hancur($id)
    {
        $x = User::max('id');
        $y = (int) substr($x, 2, 4);
        $y++;
        $z = "US" . sprintf("%04s", $y);

        User::create([
            'id' => $z,
            'username' => $this->uname,
            'password' => $this->password,
            'level' => 'Pelanggan',
        ]);

        $user = User::where('username', $this->uname)->first();

        $x2 = Pelanggan::max('id');
        $y2 = (int) substr($x2, 2, 4);
        $y2++;
        $z2 = "PK" . sprintf("%04s", $y2);

        Pelanggan::create([
            'id' => $z2,
            'user_id' => $user->id,
            'tanggal_lahir' => $this->tanggal_lahir,
            'foto' => $this->foto,
            'dokumen' => $this->ktp,
        ]);

        Register::find($id)->delete();
        $this->showModal();
    }

    public function batal()
    {
        // dd('batal');
    }

    public function showConfirmation($id)
    {
        $this->emit("swal:confirm", [
            'icon'        => 'warning',
            'title'       => 'Yakin konfirmasi?',
            'text'        => "Mohon periksa data sebelum konfirmasi!",
            'confirmText' => 'Ya, konfirmasi!',
            'method'      => 'appointments:delete',
            'params'      => $id, // optional, send params to success confirmation
            'callback'    => '', // optional, fire event if no confirmed
        ]);
    }

    public function showConfirmation2($id)
    {
        $this->emit("swal:confirm2", [
            'icon'        => 'warning',
            'title'       => 'Yakin menolak?',
            'text'        => "Mohon periksa data sebelum menolak!",
            'confirmText' => 'Ya, tolak!',
            'method'      => 'appointments:delete',
            'params'      => $id, // optional, send params to success confirmation
            'callback'    => '', // optional, fire event if no confirmed
        ]);
    }

    public function showModal()
    {
        $this->emit('swal:modal', [
            'icon'  => 'success',
            'title' => 'Berhasil!!!',
            'text'  => "Data Pelanggan $this->uname berhasil diverifikasi",
        ]);
    }

    public function berhasil()
    {
        return redirect()->to('/pelanggan/data');
    }

    public function render()
    {
        return view('livewire.registrasi.lihat2')->extends('layouts.admin', ['title' => 'Lihat Registrasi Pelanggan'])->section('content');
    }
}
