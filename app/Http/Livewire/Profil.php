<?php

namespace App\Http\Livewire;

use App\Models\Pekerja;
use App\Models\Pelanggan;
use App\Models\Sertifikat;
use App\Models\User;
use App\Models\Validasi;
use Livewire\Component;

class Profil extends Component
{
    public $pekerja;
    public $pelanggan;
    public $sd;
    public $smp;
    public $sma;
    public $cek;
    public $berhasil;
    public $gagal;

    public function mount()
    {
        $this->user = User::find(auth()->user()->id);
        if (auth()->user()->level == 'Pekerja') {
            $this->pekerja = Pekerja::find(auth()->user()->pekerja->id);
            $this->sd = explode("-", $this->user->pekerja->ket_sd);
            $this->smp = explode("-", $this->user->pekerja->ket_smp);
            $this->sma = explode("-", $this->user->pekerja->ket_sma);
            $this->cek = Validasi::where('pekerja_id', auth()->user()->pekerja->id)->where('status', 'Pending')->exists();
            $this->berhasil = Validasi::where('pekerja_id', auth()->user()->pekerja->id)->where('status', 'Konfirmasi')->exists();
            $this->gagal = Validasi::where('pekerja_id', auth()->user()->pekerja->id)->where('status', 'Ditolak')->exists();
        } else {
            $this->pelanggan = Pelanggan::find(auth()->user()->pelanggan->id);
        }
    }

    public function render()
    {
        if (auth()->user()->level == 'Pelanggan') {
            return view('livewire.profil')->extends('layouts.pelanggan', ['title' => 'Profil'])->section('content');
        } else if (auth()->user()->level == 'Pekerja') {
            $ser = Sertifikat::where('pekerja_id', auth()->user()->pekerja->id)->where('status', 'Konfirmasi')->get();
            return view('livewire.profil', compact(['ser']))->extends('layouts.pekerja', ['title' => 'Profil'])->section('content');
        }
    }
}
