<?php

namespace App\Http\Livewire\Register;

use App\Models\Register;
use Carbon\Carbon;
use Illuminate\Support\Facades\Hash;
use Livewire\Component;
use Livewire\WithFileUploads;

class Pekerja extends Component
{
    use WithFileUploads;

    public $tgl;
    public $username;
    public $password;
    public $tanggal_lahir;
    public $jenis;
    public $foto;
    public $ktp;
    protected $listeners = ['berhasil'];

    public function mount()
    {
        $this->tgl = Carbon::now()->format('Y-m-d');
    }

    public function updated($field)
    {
        $this->validateOnly($field, [
            'tanggal_lahir' => 'required|before:today|after:01/01/1940|date|before:' . Carbon::now()->subYears(17)->format('d M Y') . '',
            'jenis' => 'required',
            'username' => 'required|regex:/^[\w-]*$/|unique:users,username|unique:register,username|min:6|max:12',
            'password' => 'required|min:8',
            'foto' => 'required|image|max:5000',
            'ktp' => 'required|image|max:5000',
        ]);
    }

    public function register()
    {
        $this->validate([
            'tanggal_lahir' => 'required|before:today|after:01/01/1940|date|before:' . Carbon::now()->subYears(17)->format('d M Y') . '',
            'jenis' => 'required',
            'username' => 'required|regex:/^[\w-]*$/|unique:users,username|unique:register,username|min:6|max:12',
            'password' => 'required|min:8',
            'foto' => 'required|image|max:5000',
            'ktp' => 'required|image|max:5000',
        ]);

        $foto = $this->foto->store('images/register/pekerja/foto', 'public');
        $ktp = $this->ktp->store('images/register/pekerja/ktp', 'public');

        $x = Register::max('id');
        $y = (int) substr($x, 2, 4);
        $y++;
        $z = "RG" . sprintf("%04s", $y);

        Register::create([
            'id' => $z,
            'username' => $this->username,
            'password' => Hash::make($this->password),
            'tanggal_lahir' => $this->tanggal_lahir,
            'jenis' => $this->jenis,
            'level' => 'Pekerja',
            'foto' => $foto,
            'dokumen' => $ktp
        ]);

        $this->showModal();
    }


    public function showModal()
    {
        $this->emit('swal:modal', [
            'icon'  => 'success',
            'title' => 'Berhasil!!!',
            'text'  => "Data anda sedang kami verifikasi, mohon tunggu sampai 1x24 jam",
        ]);
    }

    public function berhasil()
    {
        return redirect()->to('/');
    }

    public function render()
    {
        return view('livewire.register.pekerja')->extends('layouts.login', ['title' => 'Daftar Pekerja'])->section('content');
    }
}
