<?php

namespace App\Http\Livewire\Register;

use App\Models\Register;
use Livewire\Component;
use Livewire\WithPagination;

class Data2 extends Component
{
    use WithPagination;

    public $search = '';
    public $perPage = 5;
    protected $paginationTheme = 'bootstrap';

    public function updatingSearch()
    {
        $this->resetPage();
    }

    public function render()
    {
        $registrasi = Register::where('level', 'Pelanggan')->where('username', 'like', '%' . $this->search . '%')->paginate($this->perPage);
        return view('livewire.register.data2', compact(['registrasi']))->extends('layouts.admin', ['title' => 'Data Registrasi Pelanggan'])->section('content');
    }
}
