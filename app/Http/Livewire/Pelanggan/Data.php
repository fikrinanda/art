<?php

namespace App\Http\Livewire\Pelanggan;

use App\Models\Pelanggan;
use App\Models\User;
use Illuminate\Support\Facades\Storage;
use Livewire\Component;
use Livewire\WithPagination;

class Data extends Component
{
    use WithPagination;

    public $search = '';
    public $perPage = 5;
    protected $paginationTheme = 'bootstrap';
    protected $listeners = ['yakin' => 'hancur', 'batal'];

    public function updatingSearch()
    {
        $this->resetPage();
    }

    public function hapus($id)
    {
        $user = User::find($id);
        $this->showConfirmation($user->id, $user->pelanggan->nama);
    }

    public function hancur($id)
    {
        $user = User::find($id);
        $nama = $user->pelanggan->nama;
        $pelanggan = Pelanggan::where('user_id', $id)->first();
        Storage::disk('public')->delete($pelanggan->foto);
        Storage::disk('public')->delete($pelanggan->dokumen);
        $user->delete();
        $this->showModal($nama);
    }

    public function batal()
    {
        // dd('batal');
    }

    public function showModal($nama)
    {
        $this->emit('swal:modal', [
            'icon'  => 'success',
            'title' => 'Berhasil!!!',
            'text'  => "Data Pelanggan $nama berhasil dihapus",
        ]);
    }

    public function showAlert()
    {
        $this->emit('swal:alert', [
            'type'    => 'success',
            'title'   => 'This is a success alert!!',
            'timeout' => 5000
        ]);
    }

    public function showConfirmation($id, $nama)
    {
        $this->emit("swal:confirm", [
            'icon'        => 'warning',
            'title'       => "Yakin menghapus Pelanggan $nama?",
            'text'        => "Setalah dihapus, anda tidak dapat mengembalikan data ini!",
            'confirmText' => 'Ya, hapus!',
            'method'      => 'appointments:delete',
            'params'      => $id, // optional, send params to success confirmation
            'callback'    => '', // optional, fire event if no confirmed
        ]);
    }

    public function render()
    {
        $pelanggan = Pelanggan::selectRaw('pelanggan.*')->join('users', 'users.id', '=', 'pelanggan.user_id')->where('nama', 'like', '%' . $this->search . '%')->orWhere('username', 'like', '%' . $this->search . '%')->paginate($this->perPage);
        return view('livewire.pelanggan.data', compact(['pelanggan']))->extends('layouts.admin', ['title' => 'Data Pelanggan'])->section('content');
    }
}
