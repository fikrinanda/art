<?php

namespace App\Http\Livewire\Pelanggan;

use App\Models\Pelanggan;
use App\Models\User;
use Livewire\Component;
use Illuminate\Support\Str;

class Detail extends Component
{
    public $pelanggan;
    public $i;
    public $user;

    public function mount($username)
    {
        $test = User::where('username', $username)->exists();
        if ($test) {
            $user = User::where('username', $username)->first();
            $this->pelanggan = Pelanggan::where('user_id', $user->id)->first();
            $this->i = $user->pelanggan->id;
        } else {
            abort('404');
        }
    }

    public function render()
    {
        return view('livewire.pelanggan.detail')->extends('layouts.pekerja', ['title' => 'Pelanggan'])->section('content');
    }
}
