<?php

namespace App\Http\Livewire\Pelanggan;

use App\Models\Pelanggan;
use App\Models\User;
use Livewire\Component;

class Lihat extends Component
{
    public $nama;
    public $jenis_kelamin;
    public $tanggal_lahir;
    public $alamat;
    public $foto;
    public $ktp;
    public $uname;
    public $password;

    public function mount($username)
    {
        $user = User::where('username', $username)->first();

        if ($user) {
            $pekerja = Pelanggan::where('user_id', $user->id)->first();
            if ($pekerja) {
                $this->nama = $pekerja->nama;
                $this->jenis_kelamin = $pekerja->jenis_kelamin;
                $this->tanggal_lahir = $pekerja->tanggal_lahir;
                $this->alamat = $pekerja->alamat;
                $this->foto = $pekerja->foto;
                $this->ktp = $pekerja->dokumen;
                $this->uname = $user->username;
                $this->password = $user->password;
            } else {
                abort('404');
            }
        } else {
            abort('404');
        }
    }

    public function render()
    {
        return view('livewire.pelanggan.lihat')->extends('layouts.admin', ['title' => 'Lihat Pelanggan'])->section('content');
    }
}
