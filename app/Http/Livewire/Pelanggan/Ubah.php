<?php

namespace App\Http\Livewire\Pelanggan;

use App\Models\Pelanggan;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;
use Livewire\Component;
use Livewire\WithFileUploads;

class Ubah extends Component
{
    use WithFileUploads;

    public $nama;
    public $alamat;
    public $jenis_kelamin;
    public $tanggal_lahir;
    public $foto;
    public $foto2;
    public $ktp;
    public $ktp2;
    public $i;
    public $tgl;
    protected $listeners = ['berhasil'];

    public function mount($username)
    {
        $this->tgl = Carbon::now()->format('Y-m-d');
        $user = User::where('username', $username)->first();

        if ($user) {
            $pelanggan = Pelanggan::where('user_id', $user->id)->first();
            if ($pelanggan) {
                $this->i = $pelanggan->id;
                $this->nama = $pelanggan->nama;
                $this->jenis_kelamin = $pelanggan->jenis_kelamin;
                $this->tanggal_lahir = Carbon::parse($pelanggan->tanggal_lahir)->format('Y-m-d');
                $this->alamat = $pelanggan->alamat;
                $this->foto = $pelanggan->foto;
                $this->ktp = $pelanggan->dokumen;
            } else {
                abort('404');
            }
        } else {
            abort('404');
        }
    }

    public function updated($field)
    {
        $this->validateOnly($field, [
            'nama' => 'required|regex:/^([^0-9]*)$/|min:3',
            'alamat' => 'required|min:5',
            'jenis_kelamin' => 'required',
            'tanggal_lahir' => 'required|before:today|after:01/01/1940|date|before:' . Carbon::now()->subYears(17)->format('d M Y') . '',
            // 'foto' => 'required|image|max:5000',
        ]);
    }

    public function ubah()
    {
        $this->validate([
            'nama' => 'required|regex:/^([^0-9]*)$/|min:3',
            'alamat' => 'required|min:5',
            'jenis_kelamin' => 'required',
            'tanggal_lahir' => 'required|before:today|after:01/01/1940|date|before:' . Carbon::now()->subYears(17)->format('d M Y') . '',
            // 'foto' => 'required|image|max:5000',
        ]);

        if ($this->foto2) {
            Storage::disk('public')->delete($this->foto);
            $foto2 = $this->foto2->store('images/pelanggan/foto', 'public');
        } else {
            $foto2 = $this->foto ?? null;
        }
        if ($this->ktp2) {
            Storage::disk('public')->delete($this->ktp);
            $ktp2 = $this->ktp2->store('images/pelanggan/ktp', 'public');
        } else {
            $ktp2 = $this->ktp ?? null;
        }

        Pelanggan::where('id', $this->i)->update([
            'nama' => $this->nama,
            'jenis_kelamin' => $this->jenis_kelamin,
            'tanggal_lahir' => $this->tanggal_lahir,
            'alamat' => $this->alamat,
            'foto' => $foto2,
            'dokumen' => $ktp2,
        ]);

        $this->showModal();
    }

    public function showModal()
    {
        $this->emit('swal:modal', [
            'icon'  => 'success',
            'title' => 'Berhasil!!!',
            'text'  => "Data Pelanggan $this->nama berhasil diubah",
        ]);
    }

    public function berhasil()
    {
        return redirect()->to('/pelanggan/data');
    }

    public function render()
    {
        return view('livewire.pelanggan.ubah')->extends('layouts.admin', ['title' => 'Ubah Pelanggan'])->section('content');
    }
}
