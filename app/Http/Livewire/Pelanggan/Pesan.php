<?php

namespace App\Http\Livewire\Pelanggan;

use App\Models\Pekerja;
use App\Models\Pesan as ModelsPesan;
use App\Models\User;
use Carbon\Carbon;
use Livewire\Component;

class Pesan extends Component
{
    public $pekerja;
    public $mulai;
    public $selesai;
    public $nama;
    protected $listeners = ['berhasil'];

    public function mount($username)
    {
        $test = User::where('username', $username)->where('level', 'Pekerja')->exists();
        if ($test) {
            $user = User::where('username', $username)->first();
            $this->pekerja = Pekerja::find($user->pekerja->id);
            $this->nama = $this->pekerja->user->username;
        } else {
            abort('404');
        }
    }

    public function pesan()
    {
        $x = ModelsPesan::max('id');
        $y = (int) substr($x, 2, 4);
        $y++;
        $z = "PS" . sprintf("%04s", $y);

        ModelsPesan::create([
            'id' => $z,
            'pelanggan_id' => auth()->user()->pelanggan->id,
            'pekerja_id' => $this->pekerja->id,
            'status' => 'Pending',
            'dibaca' => 'Belum'
        ]);

        $this->showModal();
    }

    public function showModal()
    {
        $this->emit('swal:modal', [
            'icon'  => 'success',
            'title' => 'Berhasil!!!',
            'text'  => "Berhasil pesan pekerja",
        ]);
    }

    public function berhasil()
    {
        return redirect()->to("/pekerja/detail/$this->nama");
    }

    public function render()
    {
        return view('livewire.pelanggan.pesan')->extends('layouts.pelanggan', ['title' => 'Pesan Pekerja'])->section('content');
    }
}
