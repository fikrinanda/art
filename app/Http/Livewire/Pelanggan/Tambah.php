<?php

namespace App\Http\Livewire\Pelanggan;

use App\Models\Pelanggan;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Hash;
use Livewire\Component;
use Livewire\WithFileUploads;

class Tambah extends Component
{
    use WithFileUploads;

    public $nama;
    public $jenis_kelamin;
    public $tanggal_lahir;
    public $alamat;
    public $foto;
    public $ktp;
    public $username;
    public $password;
    public $tgl;
    public $email;
    public $no_hp;
    public $kota;
    protected $listeners = ['berhasil'];

    public function mount()
    {
        $this->tgl = Carbon::now()->format('Y-m-d');
    }

    public function updated($field)
    {
        $this->validateOnly($field, [
            'nama' => 'required|regex:/^([^0-9]*)$/|min:3',
            'jenis_kelamin' => 'required',
            'tanggal_lahir' => 'required|before:today|after:01/01/1940|date|before:' . Carbon::now()->subYears(17)->format('d M Y') . '',
            'alamat' => 'required|min:5',
            'username' => 'required|regex:/^[\w-]*$/|unique:users,username|min:6|max:12',
            'password' => 'required|min:8',
            'foto' => 'required|image|max:5000',
            'ktp' => 'required|image|max:5000',
        ]);
    }

    public function tambah()
    {
        $this->validate([
            'nama' => 'required|regex:/^([^0-9]*)$/|min:3',
            'jenis_kelamin' => 'required',
            'tanggal_lahir' => 'required|before:today|after:01/01/1940|date|before:' . Carbon::now()->subYears(17)->format('d M Y') . '',
            'alamat' => 'required|min:5',
            'username' => 'required|regex:/^[\w-]*$/|unique:users,username|min:6|max:12',
            'password' => 'required|min:8',
            'foto' => 'required|image|max:5000',
            'ktp' => 'required|image|max:5000',
        ]);

        $foto = $this->foto->store('images/pelanggan/foto', 'public');
        $ktp = $this->ktp->store('images/pelanggan/ktp', 'public');

        $x = User::max('id');
        $y = (int) substr($x, 2, 4);
        $y++;
        $z = "US" . sprintf("%04s", $y);

        User::create([
            'id' => $z,
            'username' => $this->username,
            'password' => Hash::make($this->password),
            'level' => 'Pelanggan',
        ]);

        $user = User::where('username', $this->username)->first();

        $x2 = Pelanggan::max('id');
        $y2 = (int) substr($x2, 2, 4);
        $y2++;
        $z2 = "PL" . sprintf("%04s", $y2);

        Pelanggan::create([
            'id' => $z2,
            'user_id' => $user->id,
            'jenis_kelamin' => $this->jenis_kelamin,
            'tanggal_lahir' => $this->tanggal_lahir,
            'nama' => $this->nama,
            'alamat' => $this->alamat,
            'kota' => $this->kota,
            'email' => $this->email,
            'no_hp' => $this->no_hp,
            'foto' => $foto,
            'dokumen' => $ktp
        ]);

        $this->showModal();
    }

    public function showModal()
    {
        $this->emit('swal:modal', [
            'icon'  => 'success',
            'title' => 'Berhasil!!!',
            'text'  => "Data Pelanggan $this->nama berhasil ditambahkan",
        ]);
    }

    public function berhasil()
    {
        return redirect()->to('/pelanggan/data');
    }

    public function render()
    {
        return view('livewire.pelanggan.tambah')->extends('layouts.admin', ['title' => 'Tambah Pelanggan'])->section('content');
    }
}
