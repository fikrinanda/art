<?php

namespace App\Http\Livewire;

use App\Models\Obrolan;
use Livewire\Component;

class Ang2 extends Component
{
    public $angka;

    public function mount()
    {
        if (Obrolan::where('penerima', auth()->user()->id)->where('is_pelanggan', 'Belum')->exists()) {
            $this->angka = Obrolan::where('penerima', auth()->user()->id)->where('is_pelanggan', 'Belum')->count();
        } else {
            $this->angka = null;
        }
    }

    public function hydrate()
    {
        if (Obrolan::where('penerima', auth()->user()->id)->where('is_pelanggan', 'Belum')->exists()) {
            $this->angka = Obrolan::where('penerima', auth()->user()->id)->where('is_pelanggan', 'Belum')->count();
        } else {
            $this->angka = null;
        }
    }

    public function render()
    {
        return view('livewire.ang2');
    }
}
