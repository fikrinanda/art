<?php

namespace App\Http\Livewire;

use App\Models\Pesan;
use Livewire\Component;

class Notif extends Component
{
    public $pelanggan;

    public function mount()
    {
        $this->pelanggan = Pesan::where('pekerja_id', auth()->user()->pekerja->id)->get();
    }

    public function hydrate()
    {
        $this->pelanggan = Pesan::where('pekerja_id', auth()->user()->pekerja->id)->get();
    }

    public function halo()
    {
        Pesan::where('pekerja_id', auth()->user()->pekerja->id)->update([
            'is_pekerja' => 'Sudah'
        ]);
    }

    public function render()
    {
        return view('livewire.notif');
    }
}
