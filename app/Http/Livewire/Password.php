<?php

namespace App\Http\Livewire;

use Illuminate\Support\Facades\Hash;
use Livewire\Component;

class Password extends Component
{
    public $old_password;
    public $password;
    public $password_confirmation;
    protected $listeners = ['berhasil'];

    public function updated($field)
    {
        $this->validateOnly($field, [
            'old_password' => 'required',
            'password' => 'required|min:5',
            'password_confirmation' => 'required',
        ]);
    }

    public function ubah()
    {
        $this->validate([
            'old_password' => 'required',
            'password' => 'required|min:5|confirmed',
            'password_confirmation' => 'required',
        ]);

        if (Hash::check($this->old_password, auth()->user()->password)) {
            auth()->user()->update([
                'password' => Hash::make($this->password),
            ]);
            $this->show();
        } else {
            $this->old_password = '';
            $this->password = '';
            $this->password_confirmation = '';
            $this->showAlert();
        }
    }

    public function showAlert()
    {
        $this->emit('swal:alert', [
            'icon'    => 'error',
            'title'   => 'Password lama salah!!!',
            'timeout' => 3000
        ]);
    }

    public function show()
    {
        $this->emit('swal:modal', [
            'icon'  => 'success',
            'title' => 'Berhasil!!!',
            'text'  => "Password berhasil diubah",
        ]);
    }

    public function berhasil()
    {
        return redirect()->to('/log');
    }

    public function render()
    {
        if (auth()->user()->level == 'Admin') {
            return view('livewire.password')->extends('layouts.admin', ['title' => 'Ubah Password'])->section('content');
        } else if (auth()->user()->level == 'Pelanggan') {
            return view('livewire.password')->extends('layouts.pelanggan', ['title' => 'Ubah Password'])->section('content');
        } else if (auth()->user()->level == 'Pekerja') {
            return view('livewire.password')->extends('layouts.pekerja', ['title' => 'Ubah Password'])->section('content');
        }
    }
}
