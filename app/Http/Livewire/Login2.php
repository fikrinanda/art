<?php

namespace App\Http\Livewire;

use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Livewire\Component;

class Login2 extends Component
{
    public $username;
    public $password;

    public function login()
    {
        $this->validate([
            'username' => 'required',
            'password' => 'required',
        ]);

        $user = User::whereUsername($this->username)->where('level', 'Admin')->first();
        if ($user && Hash::check($this->password, $user->password)) {
            Auth::login($user);
            if ($user->level == 'Admin') {
                return redirect()->intended('/admin');
            } else {
                return back()->with('error', 'Username / Password Salah')->with($this->showModal());
            }
        } else {
            return back()->with('error', 'Username / Password Salah')->with($this->showModal());
        }
    }

    public function showModal()
    {
        $this->emit('swal:modal', [
            'icon'  => 'error',
            'title' => 'Gagal!!!',
            'text'  => "Username / Password Salah",
        ]);
    }

    public function render()
    {
        return view('livewire.login2')->extends('layouts.login', ['title' => 'Login'])->section('content');
    }
}
