<?php

namespace App\Http\Livewire;

use App\Models\Pesan;
use Livewire\Component;

class Angka2 extends Component
{
    public $angka;

    public function mount()
    {
        if (Pesan::where('pelanggan_id', auth()->user()->pelanggan->id)->where('is_pelanggan', 'Belum')->exists()) {
            $this->angka = Pesan::where('pelanggan_id', auth()->user()->pelanggan->id)->where('is_pelanggan', 'Belum')->count();
        }
    }

    public function hydrate()
    {
        if (Pesan::where('pelanggan_id', auth()->user()->pelanggan->id)->where('is_pelanggan', 'Belum')->exists()) {
            $this->angka = Pesan::where('pelanggan_id', auth()->user()->pelanggan->id)->where('is_pelanggan', 'Belum')->count();
        }
    }

    public function render()
    {
        return view('livewire.angka2');
    }
}
