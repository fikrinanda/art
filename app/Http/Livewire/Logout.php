<?php

namespace App\Http\Livewire;

use Livewire\Component;

class Logout extends Component
{
    public function mount()
    {
        if (auth()->user()->level == 'Admin') {
            auth()->logout();
            return redirect('/login');
        } else {
            auth()->logout();
            return redirect('/');
        }
    }

    public function render()
    {
        if (auth()->user()->level == 'Admin') {
            return view('livewire.logout')->extends('layouts.admin')->section('content');
        } else if (auth()->user()->level == 'Pelanggan') {
            return view('livewire.logout')->extends('layouts.pelanggan')->section('content');
        } else if (auth()->user()->level == 'Pekerja') {
            return view('livewire.logout')->extends('layouts.pekerja')->section('content');
        }
    }
}
