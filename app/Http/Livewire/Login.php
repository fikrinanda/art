<?php

namespace App\Http\Livewire;

use App\Models\Register;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Livewire\Component;

class Login extends Component
{
    public $username;
    public $password;

    public function login()
    {
        $this->validate([
            'username' => 'required',
            'password' => 'required',
        ]);

        $user = User::whereUsername($this->username)->where('level', '!=', 'Admin')->first();
        if ($user && Hash::check($this->password, $user->password)) {
            Auth::login($user);
            if ($user->level == 'Pekerja') {
                if ($user->pekerja->status == 'Tidak Aktif') {
                    return back()->with('error', 'Akun Anda Tidak Aktif')->with($this->showModal3());
                } else {
                    return redirect()->intended('/pekerja');
                }
            } else if ($user->level == 'Pelanggan') {
                return redirect()->intended('/pelanggan');
            } else {
                return back()->with('error', 'Username / Password Salah')->with($this->showModal());
            }
        } else if (($rg = Register::whereUsername($this->username)->first()) && Hash::check($this->password, $rg->password)) {
            return back()->with('error', 'Tunggu data anda selesai kami verifikasi')->with($this->showModal2());
        } else {
            return back()->with('error', 'Username / Password Salah')->with($this->showModal());
        }
    }

    public function showModal()
    {
        $this->emit('swal:modal', [
            'icon'  => 'error',
            'title' => 'Gagal!!!',
            'text'  => "Username / Password Salah",
        ]);
    }

    public function showModal3()
    {
        $this->emit('swal:modal', [
            'icon'  => 'warning',
            'title' => 'Gagal!!!',
            'text'  => "Akun anda tidak aktif",
        ]);
    }

    public function showModal2()
    {
        $this->emit('swal:modal', [
            'icon'  => 'warning',
            'title' => 'Gagal!!!',
            'text'  => "Tunggu data anda selesai kami verifikasi",
        ]);
    }

    public function render()
    {
        return view('livewire.login')->extends('layouts.login', ['title' => 'Login'])->section('content');
    }
}
