<?php

namespace App\Http\Livewire;

use App\Models\Obrolan;
use App\Models\Pekerja;
use App\Models\Pelanggan;
use App\Models\Pesan;
use App\Models\User;
use Livewire\Component;
use Illuminate\Support\Str;

class Chat extends Component
{
    public $user;
    public $i;
    public $i2;
    public $i3;
    public $pesan;
    public $ob;
    public $no = 0;
    public $temp;
    public $f;
    public $info;
    public $info2;
    public $dz = "dz-chat-user";
    protected $listeners = ['berhasil', 'yakin2' => 'hancur2', 'batal2', 'yakin3' => 'hancur3', 'batal3'];

    public function mount()
    {
        if (auth()->user()->level == 'Pelanggan') {
            $this->user = User::where('level', '!=', auth()->user()->level)->where('level', '!=', 'Admin')->get();
        } else if (auth()->user()->level == 'Pekerja') {
            $this->user = User::where('level', '!=', auth()->user()->level)->where('level', '!=', 'Admin')->get();
        }

        $this->ob = Obrolan::where(function ($q) {
            $q->where('pengirim', auth()->user()->id);
            $q->where('penerima', $this->i2);
        })->orWhere(function ($q) {
            $q->where('penerima', auth()->user()->id);
            $q->where('pengirim', $this->i2);
        })->orderBy('created_at', 'asc')->get();

        foreach ($this->user as $key => $value) {
            $this->temp[$key] = $value->id;
            $last = Obrolan::where(function ($q) {
                $q->where('pengirim', auth()->user()->id);
                $q->where('penerima', $this->temp[$this->no]);
            })->orWhere(function ($q) {
                $q->where('penerima', auth()->user()->id);
                $q->where('pengirim', $this->temp[$this->no]);
            })->latest()->first();
            $this->no = $this->no + 1;
            if (is_null($last)) {
                $this->f[$key] = null;
            } else {
                $this->f[$key] = $last->pesan;
            }
        }
    }

    public function hydrate()
    {
        $this->ob = Obrolan::where(function ($q) {
            $q->where('pengirim', auth()->user()->id);
            $q->where('penerima', $this->i2);
        })->orWhere(function ($q) {
            $q->where('penerima', auth()->user()->id);
            $q->where('pengirim', $this->i2);
        })->orderBy('created_at', 'asc')->get();

        if (auth()->user()->level == 'Pelanggan') {
            $user = User::where('level', '!=', auth()->user()->level)->where('level', '!=', 'Admin')->get();
        } else if (auth()->user()->level == 'Pekerja') {
            $user = User::where('level', '!=', auth()->user()->level)->where('level', '!=', 'Admin')->get();
        }

        $this->no = 0;
        foreach ($this->user as $key => $value) {
            $this->temp[$key] = $value->id;
            $last = Obrolan::where(function ($q) {
                $q->where('pengirim', auth()->user()->id);
                $q->where('penerima', $this->temp[$this->no]);
            })->orWhere(function ($q) {
                $q->where('penerima', auth()->user()->id);
                $q->where('pengirim', $this->temp[$this->no]);
            })->latest()->first();
            $this->no = $this->no + 1;
            if (is_null($last)) {
                $this->f[$key] = null;
            } else {
                $this->f[$key] = $last->pesan;
            }
        }
    }

    public function coba($id)
    {
        if (auth()->user()->level == 'Pelanggan') {
            $this->i = User::find($id)->pekerja->nama;
            $this->i2 = $id;
            $this->i3 = User::find($id)->pekerja->id;
        } else if (auth()->user()->level == 'Pekerja') {
            $this->i = User::find($id)->pelanggan->nama;
            $this->i2 = $id;
            $this->i3 = User::find($id)->pelanggan->id;
        }

        $this->ob = Obrolan::where(function ($q) {
            $q->where('pengirim', auth()->user()->id);
            $q->where('penerima', $this->i2);
        })->orWhere(function ($q) {
            $q->where('penerima', auth()->user()->id);
            $q->where('pengirim', $this->i2);
        })->orderBy('created_at', 'asc')->get();

        if (auth()->user()->level == 'Pelanggan') {
            $user = User::where('level', '!=', auth()->user()->level)->where('level', '!=', 'Admin')->get();
        } else if (auth()->user()->level == 'Pekerja') {
            $user = User::where('level', '!=', auth()->user()->level)->where('level', '!=', 'Admin')->get();
        }

        $this->no = 0;
        foreach ($this->user as $key => $value) {
            $this->temp[$key] = $value->id;
            $last = Obrolan::where(function ($q) {
                $q->where('pengirim', auth()->user()->id);
                $q->where('penerima', $this->temp[$this->no]);
            })->orWhere(function ($q) {
                $q->where('penerima', auth()->user()->id);
                $q->where('pengirim', $this->temp[$this->no]);
            })->latest()->first();
            $this->no = $this->no + 1;
            if (is_null($last)) {
                $this->f[$key] = null;
            } else {
                $this->f[$key] = $last->pesan;
            }
        }

        if (auth()->user()->level == 'Pelanggan') {
            Obrolan::where('penerima', auth()->user()->id)->where('pengirim', $id)->update([
                'is_pelanggan' => 'Sudah'
            ]);
        } else if (auth()->user()->level == 'Pekerja') {
            Obrolan::where('penerima', auth()->user()->id)->where('pengirim', $id)->update([
                'is_pekerja' => 'Sudah'
            ]);
        }
    }

    public function kirim()
    {
        if (auth()->user()->level == 'Pelanggan') {
            Obrolan::create([
                'pengirim' => auth()->user()->id,
                'penerima' => $this->i2,
                'hash' => Str::random(32),
                'pesan' => $this->pesan ?? null,
                'is_pelanggan' => 'Sudah',
                'is_pekerja' => 'Belum',
            ]);
        } else if (auth()->user()->level == 'Pekerja') {
            Obrolan::create([
                'pengirim' => auth()->user()->id,
                'penerima' => $this->i2,
                'hash' => Str::random(32),
                'pesan' => $this->pesan ?? null,
                'is_pelanggan' => 'Belum',
                'is_pekerja' => 'Sudah',
            ]);
        }

        $this->ob = Obrolan::where(function ($q) {
            $q->where('pengirim', auth()->user()->id);
            $q->where('penerima', $this->i2);
        })->orWhere(function ($q) {
            $q->where('penerima', auth()->user()->id);
            $q->where('pengirim', $this->i2);
        })->orderBy('created_at', 'asc')->get();

        if (auth()->user()->level == 'Pelanggan') {
            $user = User::where('level', '!=', auth()->user()->level)->where('level', '!=', 'Admin')->get();
        } else if (auth()->user()->level == 'Pekerja') {
            $user = User::where('level', '!=', auth()->user()->level)->where('level', '!=', 'Admin')->get();
        }

        $this->no = 0;
        foreach ($this->user as $key => $value) {
            $this->temp[$key] = $value->id;
            $last = Obrolan::where(function ($q) {
                $q->where('pengirim', auth()->user()->id);
                $q->where('penerima', $this->temp[$this->no]);
            })->orWhere(function ($q) {
                $q->where('penerima', auth()->user()->id);
                $q->where('pengirim', $this->temp[$this->no]);
            })->latest()->first();
            $this->no = $this->no + 1;
            if (is_null($last)) {
                $this->f[$key] = null;
            } else {
                $this->f[$key] = $last->pesan;
            }
        }

        $this->pesan = null;
    }

    public function updatedPesan()
    {
        $this->ob = Obrolan::where(function ($q) {
            $q->where('pengirim', auth()->user()->id);
            $q->where('penerima', $this->i2);
        })->orWhere(function ($q) {
            $q->where('penerima', auth()->user()->id);
            $q->where('pengirim', $this->i2);
        })->orderBy('created_at', 'asc')->get();

        if (auth()->user()->level == 'Pelanggan') {
            $user = User::where('level', '!=', auth()->user()->level)->where('level', '!=', 'Admin')->get();
        } else if (auth()->user()->level == 'Pekerja') {
            $user = User::where('level', '!=', auth()->user()->level)->where('level', '!=', 'Admin')->get();
        }

        $this->no = 0;
        foreach ($this->user as $key => $value) {
            $this->temp[$key] = $value->id;
            $last = Obrolan::where(function ($q) {
                $q->where('pengirim', auth()->user()->id);
                $q->where('penerima', $this->temp[$this->no]);
            })->orWhere(function ($q) {
                $q->where('penerima', auth()->user()->id);
                $q->where('pengirim', $this->temp[$this->no]);
            })->latest()->first();
            $this->no = $this->no + 1;
            if (is_null($last)) {
                $this->f[$key] = null;
            } else {
                $this->f[$key] = $last->pesan;
            }
        }
    }

    public function pesan2()
    {
        $x = Pesan::max('id');
        $y = (int) substr($x, 2, 4);
        $y++;
        $z = "PS" . sprintf("%04s", $y);

        Pesan::where('pekerja_id', User::find($this->i2)->pekerja->id)->where('pelanggan_id', auth()->user()->pelanggan->id)->update([
            'status' => 'Ajukan',
            'is_pelanggan' => 'Belum',
            'is_pekerja' => 'Belum',
        ]);


        $this->showModal();
    }

    public function showModal()
    {
        $this->emit('swal:modal', [
            'icon'  => 'success',
            'title' => 'Berhasil!!!',
            'text'  => "Berhasil mengajukan pesan dengan pekerja",
        ]);
    }

    public function berhasil()
    {
        return redirect()->to('/');
    }

    public function hapus()
    {
        if (auth()->user()->level == 'Pelanggan') {
            $id = User::find($this->i2)->pekerja->id;
            $this->showConfirmation(auth()->user()->pelanggan->id, $id,  "Pekerja " . Pekerja::find($id)->nama);
        } else if (auth()->user()->level == 'Pekerja') {
            $id = User::find($this->i2)->pelanggan->id;
            $this->showConfirmation2($id, auth()->user()->pekerja->id, "Pelanggan " . Pelanggan::find($id)->nama);
        }
    }

    public function hancur2($pelanggan, $pekerja)
    {
        Pesan::where('pelanggan_id', $pelanggan)->where('pekerja_id', $pekerja)->update([
            'status' => 'Batal',
            'is_pelanggan' => 'Belum',
            'is_pekerja' => 'Belum',
        ]);
        Pekerja::where('id', $pekerja)->update([
            'status' => 'Aktif'
        ]);
        $this->showModal2("Pekerja " . Pekerja::find($pekerja)->nama);
    }


    public function batal2()
    {
        // dd('batal');
    }

    public function hancur3($pelanggan, $pekerja)
    {
        Pesan::where('pelanggan_id', $pelanggan)->where('pekerja_id', $pekerja)->update([
            'status' => 'Batal',
            'is_pelanggan' => 'Belum',
            'is_pekerja' => 'Belum',
        ]);
        Pekerja::where('id', $pekerja)->update([
            'status' => 'Aktif'
        ]);
        $this->showModal2("Pelanggan " . Pelanggan::find($pelanggan)->nama);
    }


    public function batal3()
    {
        // dd('batal');
    }

    public function showModal2($nama)
    {
        $this->emit('swal:modal', [
            'icon'  => 'success',
            'title' => 'Berhasil!!!',
            'text'  => "Berhasil membatalkan pesanan dengan $nama",
        ]);
    }

    public function showConfirmation($pelanggan, $pekerja, $nama)
    {
        $this->emit("swal:confirm2", [
            'icon'        => 'warning',
            'title'       => "Yakin membatalkan pesanan dengan $nama?",
            // 'text'        => "Setalah dihapus, anda tidak dapat mengembalikan data ini!",
            'confirmText' => 'Yakin!',
            'method'      => 'appointments:delete',
            'params'      => $pelanggan, // optional, send params to success confirmation
            'params2'      => $pekerja, // optional, send params to success confirmation
            'callback'    => '', // optional, fire event if no confirmed
        ]);
    }

    public function showConfirmation2($pelanggan, $pekerja, $nama)
    {
        $this->emit("swal:confirm3", [
            'icon'        => 'warning',
            'title'       => "Yakin membatalkan pesanan dengan $nama?",
            // 'text'        => "Setalah dihapus, anda tidak dapat mengembalikan data ini!",
            'confirmText' => 'Yakin!',
            'method'      => 'appointments:delete',
            'params'      => $pelanggan, // optional, send params to success confirmation
            'params2'      => $pekerja, // optional, send params to success confirmation
            'callback'    => '', // optional, fire event if no confirmed
        ]);
    }


    public function render()
    {
        return view('livewire.chat');
    }
}
