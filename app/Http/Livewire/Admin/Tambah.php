<?php

namespace App\Http\Livewire\Admin;

use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Livewire\Component;

class Tambah extends Component
{
    public $username;
    public $password;
    protected $listeners = ['berhasil'];

    public function updated($field)
    {
        $this->validateOnly($field, [
            'username' => 'required|regex:/^[\w-]*$/|unique:users,username|min:6|max:12',
            'password' => 'required|min:8|max:40'
        ]);
    }

    public function tambah()
    {
        $this->validate([
            'username' => 'required|regex:/^[\w-]*$/|unique:users,username|min:6|max:12',
            'password' => 'required|min:8|max:40'
        ]);

        $x = User::max('id');
        $y = (int) substr($x, 2, 4);
        $y++;
        $z = "US" . sprintf("%04s", $y);

        User::create([
            'id' => $z,
            'username' => $this->username,
            'password' => Hash::make($this->password),
            'level' => 'Admin',
        ]);

        $this->showModal();
    }

    public function showModal()
    {
        $this->emit('swal:modal', [
            'icon'  => 'success',
            'title' => 'Berhasil!!!',
            'text'  => "Data Admin $this->username berhasil ditambahkan",
        ]);
    }

    public function berhasil()
    {
        return redirect()->to('/admin/data');
    }

    public function render()
    {
        return view('livewire.admin.tambah')->extends('layouts.admin', ['title' => 'Tambah Admin'])->section('content');
    }
}
