<?php

namespace App\Http\Livewire\Admin;

use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Livewire\Component;

class Ubah extends Component
{
    public $username;
    public $i;
    public $password;
    protected $listeners = ['berhasil'];

    public function mount($username)
    {
        $user = User::where('username', $username);
        if ($user->exists()) {
            $this->i = $user->first()->id;
        } else {
            abort('404');
        }
    }

    public function updated($field)
    {
        $this->validateOnly($field, [
            'username' => 'required|regex:/^[\w-]*$/|min:6|max:12|unique:users,username,' . $this->i,
            'password' => 'required|min:8|max:40'
        ]);
    }

    public function ubah()
    {
        $this->validate([
            'username' => 'required|regex:/^[\w-]*$/|min:6|max:12|unique:users,username,' . $this->i,
            'password' => 'required|min:8|max:40'
        ]);

        User::where('id', $this->i)->update([
            'username' => $this->username,
            'password' => Hash::make($this->password)
        ]);

        $this->showModal();
    }

    public function showModal()
    {
        $this->emit('swal:modal', [
            'icon'  => 'success',
            'title' => 'Berhasil!!!',
            'text'  => "Data Admin $this->username berhasil diubah",
        ]);
    }

    public function berhasil()
    {
        return redirect()->to('/admin/data');
    }

    public function render()
    {
        return view('livewire.admin.ubah')->extends('layouts.admin', ['title' => 'Ubah Admin'])->section('content');
    }
}
