<?php

namespace App\Http\Livewire\Admin;

use App\Models\Rating;
use Livewire\Component;
use Livewire\WithPagination;

class Index extends Component
{
    use WithPagination;

    public $search = '';
    public $perPage = 5;
    protected $paginationTheme = 'bootstrap';

    public function render()
    {
        $rating = Rating::selectRaw('rating.*')->join('users', 'users.id', '=', 'rating.user_id')->where('keterangan', 'like', '%' . $this->search . '%')->orWhere('username', 'like', '%' . $this->search . '%')->paginate($this->perPage);
        $count = Rating::count('*');
        $sum = Rating::sum('rating');
        $avg = $sum / $count;
        return view('livewire.admin.index', compact(['rating', 'avg']))->extends('layouts.admin', ['title' => 'Admin'])->section('content');
    }
}
