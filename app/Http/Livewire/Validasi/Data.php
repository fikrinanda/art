<?php

namespace App\Http\Livewire\Validasi;

use App\Models\Validasi;
use Livewire\Component;
use Livewire\WithPagination;

class Data extends Component
{
    use WithPagination;

    public $search = '';
    public $perPage = 5;
    protected $paginationTheme = 'bootstrap';

    public function updatingSearch()
    {
        $this->resetPage();
    }

    public function render()
    {
        $validasi = Validasi::where('nama', 'like', '%' . $this->search . '%')->paginate($this->perPage);
        return view('livewire.validasi.data', compact(['validasi']))->extends('layouts.admin', ['title' => 'Data Validasi'])->section('content');
    }
}
