<?php

namespace App\Http\Livewire\Validasi;

use App\Models\Pekerja;
use App\Models\Sertifikat;
use App\Models\Validasi;
use Livewire\Component;

class Lihat extends Component
{
    public $pekerja;
    public $validasi_id;
    public $nama;
    public $sd;
    public $smp;
    public $sma;
    public $dokumen;
    protected $listeners = ['yakin' => 'hancur', 'batal', 'berhasil', 'yakin2' => 'hancur2'];

    public function mount($validasi_id)
    {
        $this->pekerja = Validasi::where('id', $validasi_id)->first();

        $this->nama = Validasi::where('id', $validasi_id)->first()->nama;
        $this->sd = explode("-", $this->pekerja->ket_sd);
        $this->smp = explode("-", $this->pekerja->ket_smp);
        $this->sma = explode("-", $this->pekerja->ket_sma);
        $this->dokumen = Sertifikat::where('pekerja_id', $this->pekerja->pekerja_id)->get();
    }

    public function tolak()
    {
        $this->pekerja = Validasi::where('id', $this->validasi_id)->first();
        $this->sd = explode("-", $this->pekerja->ket_sd);
        $this->smp = explode("-", $this->pekerja->ket_smp);
        $this->sma = explode("-", $this->pekerja->ket_sma);
        $this->dokumen = Sertifikat::where('pekerja_id', $this->pekerja->pekerja_id)->get();
        $this->showConfirmation2($this->validasi_id);
    }

    public function hancur2($i)
    {
        $pekerja = Validasi::where('id', $this->validasi_id)->first();

        Validasi::where('id', $i)->update([
            'status' => 'Ditolak'
        ]);
        Sertifikat::where('pekerja_id', $pekerja->pekerja_id)->where('status', 'Pending')->update([
            'status' => 'Ditolak'
        ]);
        return redirect()->to('/pekerja/validasi');
    }

    public function konfirmasi()
    {
        $this->pekerja = Validasi::where('id', $this->validasi_id)->first();
        $this->sd = explode("-", $this->pekerja->ket_sd);
        $this->smp = explode("-", $this->pekerja->ket_smp);
        $this->sma = explode("-", $this->pekerja->ket_sma);
        $this->dokumen = Sertifikat::where('pekerja_id', $this->pekerja->pekerja_id)->get();
        $this->showConfirmation($this->validasi_id);
    }

    public function hancur($id)
    {
        $pekerja = Validasi::where('id', $this->validasi_id)->first();
        // dd($pekerja->nama);
        Pekerja::where('id', $pekerja->pekerja_id)->update([
            'nama' => $pekerja->nama,
            'jenis_kelamin' => $pekerja->jenis_kelamin,
            'tanggal_lahir' => $pekerja->tanggal_lahir,
            'umur' => $pekerja->umur,
            'alamat' => $pekerja->alamat,
            'kota' => $pekerja->kota,
            'email' => $pekerja->email,
            'no_hp' => $pekerja->no_hp,
            'jenis' => $pekerja->jenis,
            'foto' => $pekerja->foto,
            'dokumen' => $pekerja->dokumen,
            'sd' => $pekerja->sd,
            'smp' => $pekerja->smp,
            'sma' => $pekerja->sma,
            'harga' => $pekerja->harga,
            'ket_sd' => $pekerja->ket_sd,
            'ket_smp' => $pekerja->ket_smp,
            'ket_sma' => $pekerja->ket_sma,
        ]);

        Validasi::where('id', $id)->update([
            'status' => 'Konfirmasi'
        ]);

        Sertifikat::where('pekerja_id', $pekerja->pekerja_id)->where('status', 'Pending')->update([
            'status' => 'Konfirmasi'
        ]);
        $this->showModal();
    }

    public function batal()
    {
        $this->pekerja = Validasi::where('id', $this->validasi_id)->first();
        $this->sd = explode("-", $this->pekerja->ket_sd);
        $this->smp = explode("-", $this->pekerja->ket_smp);
        $this->sma = explode("-", $this->pekerja->ket_sma);
        $this->dokumen = Sertifikat::where('pekerja_id', $this->pekerja->pekerja_id)->get();
    }

    public function showConfirmation($id)
    {
        $this->emit("swal:confirm", [
            'icon'        => 'warning',
            'title'       => 'Yakin konfirmasi?',
            'text'        => "Mohon periksa data sebelum konfirmasi!",
            'confirmText' => 'Ya, konfirmasi!',
            'method'      => 'appointments:delete',
            'params'      => $id, // optional, send params to success confirmation
            'callback'    => '', // optional, fire event if no confirmed
        ]);
    }

    public function showConfirmation2($id)
    {
        $this->emit("swal:confirm2", [
            'icon'        => 'warning',
            'title'       => 'Yakin menolak?',
            'text'        => "Mohon periksa data sebelum menolak!",
            'confirmText' => 'Ya, tolak!',
            'method'      => 'appointments:delete',
            'params'      => $id, // optional, send params to success confirmation
            'callback'    => '', // optional, fire event if no confirmed
        ]);
    }

    public function showModal()
    {
        $this->pekerja = Validasi::where('id', $this->validasi_id)->first();
        $this->sd = explode("-", $this->pekerja->ket_sd);
        $this->smp = explode("-", $this->pekerja->ket_smp);
        $this->sma = explode("-", $this->pekerja->ket_sma);
        $this->dokumen = Sertifikat::where('pekerja_id', $this->pekerja->pekerja_id)->get();
        $this->emit('swal:modal', [
            'icon'  => 'success',
            'title' => 'Berhasil!!!',
            'text'  => "Data Pekerja $this->nama berhasil diverifikasi",
        ]);
    }

    public function berhasil()
    {
        return redirect()->to('/pekerja/validasi');
    }

    public function render()
    {
        return view('livewire.validasi.lihat')->extends('layouts.admin', ['title' => 'Lihat Validasi'])->section('content');
    }
}
