<?php

namespace App\Http\Livewire\Riwayat;

use App\Models\Pesan;
use Livewire\Component;

class Data extends Component
{
    public function render()
    {
        if (auth()->user()->level == 'Pelanggan') {
            $riwayat = Pesan::where('pelanggan_id', auth()->user()->pelanggan->id)->get();
            return view('livewire.riwayat.data', compact('riwayat'))->extends('layouts.pelanggan', ['title' => 'Riwayat'])->section('content');
        } else {
            $riwayat = Pesan::where('pekerja_id', auth()->user()->pekerja->id)->get();
            return view('livewire.riwayat.data', compact('riwayat'))->extends('layouts.pekerja', ['title' => 'Riwayat'])->section('content');
        }
    }
}
