<div>
    <div class="page-links">
        <a href="/">Login</a><a href="/daftar/pekerja" class="active">Daftar Pekerja</a><a href="/daftar/pelanggan">Daftar Pelanggan</a>
    </div>
    @if(session()->get('error'))
    <div class="alert alert-warning alert-dismissible fade show with-icon" role="alert">
        {{ session('error') }}
    </div>
    @endif
    <form wire:submit.prevent="register" autocomplete="off">
        <label for="">Username</label>
        <input class="form-control" type="text" wire:model="username" placeholder="Username">
        @error('username')
        <p style="margin-bottom: 5px;">{{ $message }}</p>
        @enderror
        <label for="">Password</label>
        <input class="form-control" type="password" wire:model="password" placeholder="Password">
        @error('password')
        <p style="margin-bottom: 5px;">{{ $message }}</p>
        @enderror
        <label for="">Tanggal Lahir</label>
        <input type="date" class="form-control" wire:model="tanggal_lahir" max="{{$tgl}}">
        @error('tanggal_lahir')
        <p style="margin-bottom: 5px;">{{ $message }}</p>
        @enderror
        <label for="">Jenis</label>
        <select wire:model="jenis" class="form-control" style="margin-bottom: 14px;">
            <option hidden>Pilih Jenis</option>
            <option value="Asisten Rumah Tangga">Asisten Rumah Tangga</option>
            <option value="Security">Security</option>
            <option value="Baby Sitter">Baby Sitter</option>
            <option value="Pengasuh Orang Tua">Pengasuh Orang Tua</option>
            <option value="Sopir">Sopir</option>
        </select>
        @error('jenis')
        <p style="margin-bottom: 5px;">{{ $message }}</p>
        @enderror
        <label for="">Foto</label>
        <input type="file" wire:model="foto">
        @error('foto')
        <p style="margin-bottom: 5px;">{{ $message }}</p>
        @enderror
        <label for="">KTP</label>
        <input type="file" wire:model="ktp">
        @error('ktp')
        <p style="margin-bottom: 5px;">{{ $message }}</p>
        @enderror

        <div class="form-button">
            <button id="submit" type="submit" class="ibtn">Daftar</button>
        </div>
    </form>
</div>