<div class="content-body">
    <div class="container-fluid">
        <div class="row gutters-sm">
            <div class="col-lg-12">
                <div class="card">

                    <div class="card-body">
                        <div class="d-flex flex-column align-items-center text-center">
                            <h4 class="card-title">Konfirmasi Negosiasi</h4>
                            <img src="/storage/{{$pelanggan->foto}}" alt="Admin" width="300" height="350">
                            <div class="mt-3">
                                <h4>{{$pelanggan->nama}}</h4>
                            </div>
                        </div>
                        <br>
                        @if($pesan->status == 'Pending')
                        <h3>{{$pelanggan->nama}} telah mengajukan negosasi dengan anda, jika konfirmasi lanjut chat</h3>
                        <h3>Alamat : {{$pelanggan->alamat}}, {{$pelanggan->kota}}</h3>
                        <h3>Email : {{$pelanggan->email}}</h3>
                        <h3>No HP : {{$pelanggan->no_hp}}</h3>
                        <h4>Konfirmasi negosiasi</h4>
                        <button class="btn-sm btn-primary" wire:click="konfirmasi('{{$pesan->id}}')">Konfirmasi</button>
                        <button class="btn-sm btn-danger" wire:click="tolak('{{$pesan->id}}')">Tolak</button>
                        @elseif($pesan->status == 'Ajukan')
                        <h3>Anda telah dipesan oleh {{$pelanggan->nama}}</h3>
                        <h3>Alamat : {{$pelanggan->alamat}}, {{$pelanggan->kota}}</h3>
                        <h3>Email : {{$pelanggan->email}}</h3>
                        <h3>No HP : {{$pelanggan->no_hp}}</h3>
                        <h4>Konfirmasi pesanan</h4>
                        <button class="btn-sm btn-primary" wire:click="konfirmasi('{{$pesan->id}}')">Konfirmasi</button>
                        <button class="btn-sm btn-danger" wire:click="tolak('{{$pesan->id}}')">Tolak</button>
                        @endif
                    </div>
                </div>

            </div>
        </div>

    </div>
</div>