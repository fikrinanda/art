<div class="content-body" wire:ignore.self>
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Ubah Profil</h4>
                    </div>
                    <div class="card-body">
                        <div class="basic-form">
                            <form wire:submit.prevent="ubah" autocomplete="off">
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Username</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" style="color: black;" placeholder="Masukkan username" wire:model="username">
                                        <div>
                                            @error('username')
                                            <span class="text-danger">{{ $message }}</span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Nama</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" style="color: black;" placeholder="Masukkan nama" wire:model="nama">
                                        <div>
                                            @error('nama')
                                            <span class="text-danger">{{ $message }}</span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Jenis Kelamin</label>
                                    <div class="col-sm-9 mt-2">
                                        <label class="radio-inline mr-3"><input type="radio" wire:model="jenis_kelamin" value="Laki-laki"> Laki-laki</label>
                                        <label class="radio-inline mr-3"><input type="radio" wire:model="jenis_kelamin" value="Perempuan"> Perempuan</label>
                                        <div>
                                            @error('jenis_kelamin')
                                            <span class="text-danger">{{ $message }}</span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Tanggal Lahir</label>
                                    <div class="col-sm-9">
                                        <input type="date" class="form-control" style="color: black;" wire:model="tanggal_lahir" max="{{$tgl}}" disabled>
                                        <div>
                                            @error('tanggal_lahir')
                                            <span class="text-danger">{{ $message }}</span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Alamat</label>
                                    <div class="col-sm-9">
                                        <textarea class="form-control" rows="4" wire:model="alamat" style="color: black; resize: none;" placeholder="Masukkan alamat"></textarea>
                                        <div>
                                            @error('alamat')
                                            <span class="text-danger">{{ $message }}</span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Kota</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" style="color: black;" placeholder="Masukkan kota" wire:model="kota">
                                        <div>
                                            @error('kota')
                                            <span class="text-danger">{{ $message }}</span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Email</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" style="color: black;" placeholder="Masukkan email" wire:model="email">
                                        <div>
                                            @error('email')
                                            <span class="text-danger">{{ $message }}</span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">No HP</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" style="color: black;" placeholder="Masukkan no HP" wire:model="no_hp">
                                        <div>
                                            @error('no_hp')
                                            <span class="text-danger">{{ $message }}</span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Jenis</label>
                                    <div class="col-sm-9">
                                        <div wire:ignore>
                                            <select wire:model="jenis" class="form-control" style="color: black;">
                                                <option hidden>Pilih Jenis</option>
                                                <option value="Asisten Rumah Tangga">Asisten Rumah Tangga</option>
                                                <option value="Security">Security</option>
                                                <option value="Baby Sitter">Baby Sitter</option>
                                                <option value="Pengasuh Orang Tua">Pengasuh Orang Tua</option>
                                                <option value="Sopir">Sopir</option>
                                            </select>
                                        </div>
                                        <div>
                                            @error('jenis')
                                            <span class="text-danger">{{ $message }}</span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Harga Rp</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" style="color: black;" placeholder="Masukkan harga" wire:model="harga">
                                        <div>
                                            @error('harga')
                                            <span class="text-danger">{{ $message }}</span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">KTP</label>
                                    <div class="col-sm-9">
                                        <input type="file" style="color: black;" wire:model="ktp2">
                                        <div>
                                            @error('ktp2')
                                            <span class="text-danger">{{ $message }}</span>
                                            @enderror
                                        </div>
                                        <div wire:loading wire:target="ktp2">Uploading...</div>
                                        @if ($ktp2)
                                        <div class="mt-3" style="object-fit: cover;">
                                            <div>Photo Preview:</div>
                                            <img src="{{ $ktp2->temporaryUrl() }}" height="100" width="150">
                                        </div>
                                        @else
                                        <div class="mt-3" style="object-fit: cover;">
                                            <a href="/storage/{{$ktp}}" data-lightbox="mygallery"><img src="/storage/{{$ktp}}" height="100" width="150"></a>
                                        </div>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Foto</label>
                                    <div class="col-sm-9">
                                        <input type="file" style="color: black;" wire:model="foto2">
                                        <div>
                                            @error('foto2')
                                            <span class="text-danger">{{ $message }}</span>
                                            @enderror
                                        </div>
                                        <div wire:loading wire:target="foto2">Uploading...</div>
                                        @if ($foto2)
                                        <div class="mt-3" style="object-fit: cover;">
                                            <div>Photo Preview:</div>
                                            <a href="/storage/{{$foto2->temporaryUrl()}}" data-lightbox="mygallery"><img src="{{ $foto2->temporaryUrl() }}" height="150" width="100"></a>
                                        </div>
                                        @else
                                        <div class="mt-3" style="object-fit: cover;">
                                            <a href="/storage/{{$foto}}" data-lightbox="mygallery"><a href="/storage/{{$foto}}" data-lightbox="mygallery"><img src="/storage/{{$foto}}" height="150" width="100"></a>
                                        </div>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">SD</label>
                                    <div class="col-sm-2">
                                        <input type="text" class="form-control" style="color: black;" wire:model="ket_sd" placeholder="Nama">
                                        <div>
                                            @error('ket_sd')
                                            <span class="text-danger">{{ $message }}</span>
                                            @enderror
                                        </div>
                                        <div wire:loading wire:target="sd2">Uploading...</div>
                                        @if ($sd2)
                                        <div class="mt-3" style="object-fit: cover;">
                                            <div>Photo Preview:</div>
                                            <img src="{{ $sd2->temporaryUrl() }}" height="150" width="100">
                                        </div>
                                        @else
                                        @if($sd != null)
                                        <div class="mt-3" style="object-fit: cover;">
                                            <a href="/storage/{{$sd}}" data-lightbox="mygallery"><img src="/storage/{{$sd}}" height="150" width="100"></a>
                                        </div>
                                        @endif
                                        @endif
                                    </div>
                                    <div class="col-sm-2">
                                        <input type="text" class="form-control" style="color: black;" wire:model="tahun_sd1" placeholder="Tahun">
                                        <div>
                                            @error('tahun_sd1')
                                            <span class="text-danger">{{ $message }}</span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="d-flex align-content-center justify-content-center mt-3" style="color: black;">/</div>
                                    <div class="col-sm-2">
                                        <input type="text" class="form-control" style="color: black;" wire:model="tahun_sd2" placeholder="Tahun">
                                        <div>
                                            @error('tahun_sd2')
                                            <span class="text-danger">{{ $message }}</span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-sm-2">
                                        <input type="file" style="color: black;" wire:model="sd2">
                                        <div>
                                            @error('sd2')
                                            <span class="text-danger">{{ $message }}</span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">SMP</label>
                                    <div class="col-sm-2">
                                        <input type="text" class="form-control" style="color: black;" wire:model="ket_smp" placeholder="Nama">
                                        <div>
                                            @error('ket_smp')
                                            <span class="text-danger">{{ $message }}</span>
                                            @enderror
                                        </div>
                                        <div wire:loading wire:target="smp2">Uploading...</div>
                                        @if ($smp2)
                                        <div class="mt-3" style="object-fit: cover;">
                                            <div>Photo Preview:</div>
                                            <img src="{{ $smp2->temporaryUrl() }}" height="150" width="100">
                                        </div>
                                        @else
                                        @if($smp != null)
                                        <div class="mt-3" style="object-fit: cover;">
                                            <a href="/storage/{{$smp}}" data-lightbox="mygallery"><img src="/storage/{{$smp}}" height="150" width="100"></a>
                                        </div>
                                        @endif
                                        @endif
                                    </div>
                                    <div class="col-sm-2">
                                        <input type="text" class="form-control" style="color: black;" wire:model="tahun_smp1" placeholder="Tahun">
                                        <div>
                                            @error('tahun_smp1')
                                            <span class="text-danger">{{ $message }}</span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="d-flex align-content-center justify-content-center mt-3" style="color: black;">/</div>
                                    <div class="col-sm-2">
                                        <input type="text" class="form-control" style="color: black;" wire:model="tahun_smp2" placeholder="Tahun">
                                        <div>
                                            @error('tahun_smp2')
                                            <span class="text-danger">{{ $message }}</span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-sm-2">
                                        <input type="file" style="color: black;" wire:model="smp2">
                                        <div>
                                            @error('smp2')
                                            <span class="text-danger">{{ $message }}</span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">SMA</label>
                                    <div class="col-sm-2">
                                        <input type="text" class="form-control" style="color: black;" wire:model="ket_sma" placeholder="Nama">
                                        <div>
                                            @error('ket_sma')
                                            <span class="text-danger">{{ $message }}</span>
                                            @enderror
                                        </div>
                                        <div wire:loading wire:target="sma2">Uploading...</div>
                                        @if ($sma2)
                                        <div class="mt-3" style="object-fit: cover;">
                                            <div>Photo Preview:</div>
                                            <img src="{{ $sma2->temporaryUrl() }}" height="150" width="100">
                                        </div>
                                        @else
                                        @if($sma != null)
                                        <div class="mt-3" style="object-fit: cover;">
                                            <a href="/storage/{{$sma}}" data-lightbox="mygallery"><img src="/storage/{{$sma}}" height="150" width="100"></a>
                                        </div>
                                        @endif
                                        @endif
                                    </div>
                                    <div class="col-sm-2">
                                        <input type="text" class="form-control" style="color: black;" wire:model="tahun_sma1" placeholder="Tahun">
                                        <div>
                                            @error('tahun_sma1')
                                            <span class="text-danger">{{ $message }}</span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="d-flex align-content-center justify-content-center mt-3" style="color: black;">/</div>
                                    <div class="col-sm-2">
                                        <input type="text" class="form-control" style="color: black;" wire:model="tahun_sma2" placeholder="Tahun">
                                        <div>
                                            @error('tahun_sma2')
                                            <span class="text-danger">{{ $message }}</span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-sm-2">
                                        <input type="file" style="color: black;" wire:model="sma2">
                                        <div>
                                            @error('sma2')
                                            <span class="text-danger">{{ $message }}</span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>

                                @foreach($ser as $index => $data)
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Dokumen Pendukung {{$index+1}}</label>
                                    <div class="col-sm-2">
                                        <input type="text" class="form-control" style="color: black;" placeholder="Keterangan" wire:model="z.{{$index}}.keterangan">
                                        <div>
                                            @error('z.' . $index . '.keterangan')
                                            <span class="text-danger">{{ $message }}</span>
                                            @enderror
                                        </div>
                                        <div wire:loading wire:target="s.{{$index}}.sertifikat">Uploading...</div>
                                        @if (isset($s[$index]['sertifikat']))
                                        <div class="mt-3" style="object-fit: cover;">
                                            <div>Photo Preview:</div>
                                            <img src="{{ $s[$index]['sertifikat']->temporaryUrl() }}" height="150" width="100">
                                        </div>
                                        @elseif (!is_null($data->sertifikat))
                                        <div class="mt-3" style="object-fit: cover;">
                                            <!-- <div>Photo Preview:</div> -->
                                            <a href="/storage/{{$z[$index]['sertifikat']}}" data-lightbox="mygallery"><img src="/storage/{{$z[$index]['sertifikat']}}" height="150" width="100"></a>
                                        </div>
                                        @endif
                                    </div>
                                    <div class="col-sm-4">
                                        <input type="file" style="color: black;" wire:model="s.{{$index}}.sertifikat">
                                        <div>
                                            @error('s.' . $index . '.sertifikat')
                                            <span class="text-danger">{{ $message }}</span>
                                            @enderror
                                        </div>
                                    </div>
                                    @if(!is_null($data->sertifikat))
                                    <div class="col-sm-2">
                                        <button type="submit" class="btn btn-danger mt-3 float-right" wire:click.prevent="hapus('{{$data->hash}}')">-</button>
                                    </div>
                                    @endif
                                </div>
                                @endforeach

                                @foreach($sertifikat as $index => $orderTgs)
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Dokumen Pendukung {{$index+$shin+1}}</label>
                                    <div class="col-sm-2">
                                        <input type="text" class="form-control" style="color: black;" placeholder="Keterangan" wire:model="sertifikat.{{$index}}.keterangan">
                                        <div>
                                            @error('sertifikat.' . $index . '.keterangan')
                                            <span class="text-danger">{{ $message }}</span>
                                            @enderror
                                        </div>
                                        <div wire:loading wire:target="sertifikat.{{$index}}.gambar">Uploading...</div>
                                        @if (isset($sertifikat[$index]['gambar']))
                                        <div class="mt-3" style="object-fit: cover;">
                                            <div>Photo Preview:</div>
                                            <img src="{{ $sertifikat[$index]['gambar']->temporaryUrl() }}" height="150" width="100">
                                        </div>
                                        @endif
                                    </div>
                                    <div class="col-sm-4">
                                        <input type="file" style="color: black;" wire:model="sertifikat.{{$index}}.gambar">
                                        <div>
                                            @error('sertifikat.' . $index . '.gambar')
                                            <span class="text-danger">{{ $message }}</span>
                                            @enderror
                                        </div>
                                    </div>
                                    @if(count($this->sertifikat) > 1)
                                    <div class="col-sm-2">
                                        <button type="submit" class="btn btn-danger btn-xs" wire:click.prevent="remove({{$index}})">-</button>
                                    </div>
                                    @endif
                                </div>
                                @endforeach
                                <div class="d-flex justify-content-end">
                                    <button type="submit" class="btn btn-primary btn-sm mb-3" wire:click.prevent="add">+</button>
                                </div>
                        </div>
                        <div class="d-flex justify-content-end">
                            <button type="submit" class="btn btn-primary">Ubah</button>
                        </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
</div>