<div class="content-body">
    <div class="container-fluid">
        <!-- Vectormap -->
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Data Pekerja</h4>
                        <div class="d-sm-flex align-items-center">
                            <a href="/pekerja/tambah" class="btn btn-outline-primary rounded">Tambah</a>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="dataTables_wrapper d-flex justify-content-between" wire:ignore>
                            <div class="dataTables_length"><label>Show <select wire:model="perPage" class="default-select">
                                        <option value="5">5</option>
                                        <option value="10">10</option>
                                        <option value="15">15</option>
                                        <option value="20">20</option>
                                    </select></label></div>
                            <div class="dataTables_filter"><label>Search&nbsp;:&nbsp;<input type="search" wire:model="search"></label></div>
                        </div>
                        @if(count($pekerja) > 0)
                        <div class="table-responsive">
                            <table class="table table-responsive-md">
                                <thead>
                                    <tr>
                                        <th class="width80">No</th>
                                        <th wire:click="sortBy('pekerja.id')" style="cursor: pointer;">
                                            ID
                                            @include('partials._sort-icon',['field'=>'pekerja.id'])</th>
                                        <th wire:click="sortBy('username')" style="cursor: pointer;">
                                            Username
                                            @include('partials._sort-icon',['field'=>'username'])
                                        </th>
                                        <th wire:click="sortBy('nama')" style="cursor: pointer;">
                                            Nama
                                            @include('partials._sort-icon',['field'=>'nama'])
                                        </th>
                                        <th>Status</th>
                                        <th>Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($pekerja as $key => $p)
                                    <tr>
                                        <td style="width: 10%;"><strong>{{$pekerja->firstItem() + $key}}</strong></td>
                                        <td style="width: 15%;">{{$p->id}}</td>
                                        <td style="width: 15%;">{{$p->user->username}}</td>
                                        <td style="width: 15%;">{{$p->nama}}</td>
                                        <td style="width: 15%;">
                                            <div>
                                                <select wire:model="s.{{ $key }}">
                                                    <option value="Aktif">Aktif</option>
                                                    <option value="Tidak Aktif">Tidak Aktif</option>
                                                    <option value="Dipesan">Dipesan</option>
                                                </select>
                                            </div>
                                        </td>
                                        <td style="width: 15%;">
                                            <div class="d-flex">
                                                <a href="/pekerja/lihat/{{$p->user->username}}" class="btn btn-success shadow sharp mr-1" data-toggle="tooltip" data-placement="top" title="Lihat"><i class="fa fa-info-circle"></i></a>
                                                <a href="/pekerja/ubah/{{$p->user->username}}" class="btn btn-warning shadow sharp mr-1" data-toggle="tooltip" data-placement="top" title="Ubah"><i class="fa fa-edit"></i></a>
                                                <button wire:click="hapus('{{$p->user->id}}')" class="btn btn-danger shadow sharp mr-1" data-toggle="tooltip" data-placement="top" title="Hapus"><i class="fa fa-trash"></i></button>
                                            </div>
                                        </td>
                                    </tr>
                                    @endforeach

                                </tbody>
                            </table>
                        </div>
                        <div class="mt-3">
                            {{$pekerja->links()}}
                        </div>
                        @else
                        <h1>Tidak ada data</h1>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>