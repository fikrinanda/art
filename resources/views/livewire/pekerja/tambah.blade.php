<div class="content-body">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Tambah Pekerja</h4>
                    </div>
                    <div class="card-body">
                        <div class="basic-form">
                            <form wire:submit.prevent="tambah" autocomplete="off">
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Nama</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" style="color: black;" placeholder="Masukkan nama" wire:model="nama">
                                        <div>
                                            @error('nama')
                                            <span class="text-danger">{{ $message }}</span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Jenis Kelamin</label>
                                    <div class="col-sm-9 mt-2">
                                        <label class="radio-inline mr-3"><input type="radio" wire:model="jenis_kelamin" value="Laki-laki"> Laki-laki</label>
                                        <label class="radio-inline mr-3"><input type="radio" wire:model="jenis_kelamin" value="Perempuan"> Perempuan</label>
                                        <div>
                                            @error('jenis_kelamin')
                                            <span class="text-danger">{{ $message }}</span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Tanggal Lahir</label>
                                    <div class="col-sm-9">
                                        <input type="date" class="form-control" style="color: black;" wire:model="tanggal_lahir" max="{{$tgl}}">
                                        <div>
                                            @error('tanggal_lahir')
                                            <span class="text-danger">{{ $message }}</span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Alamat</label>
                                    <div class="col-sm-9">
                                        <textarea class="form-control" rows="4" wire:model="alamat" style="color: black; resize: none;" placeholder="Masukkan alamat"></textarea>
                                        <div>
                                            @error('alamat')
                                            <span class="text-danger">{{ $message }}</span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Kota</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" style="color: black;" placeholder="Masukkan kota" wire:model="kota">
                                        <div>
                                            @error('kota')
                                            <span class="text-danger">{{ $message }}</span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Email</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" style="color: black;" placeholder="Masukkan email" wire:model="email">
                                        <div>
                                            @error('email')
                                            <span class="text-danger">{{ $message }}</span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">No HP</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" style="color: black;" placeholder="Masukkan no HP" wire:model="no_hp">
                                        <div>
                                            @error('no_hp')
                                            <span class="text-danger">{{ $message }}</span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Jenis</label>
                                    <div class="col-sm-9">
                                        <div wire:ignore>
                                            <select wire:model="jenis" class="form-control" style="color: black;">
                                                <option hidden>Pilih Jenis</option>
                                                <option value="Asisten Rumah Tangga">Asisten Rumah Tangga</option>
                                                <option value="Security">Security</option>
                                                <option value="Baby Sitter">Baby Sitter</option>
                                                <option value="Pengasuh Orang Tua">Pengasuh Orang Tua</option>
                                                <option value="Sopir">Sopir</option>
                                            </select>
                                        </div>
                                        <div>
                                            @error('jenis')
                                            <span class="text-danger">{{ $message }}</span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Harga</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" style="color: black;" placeholder="Masukkan harga" wire:model="harga">
                                        <div>
                                            @error('harga')
                                            <span class="text-danger">{{ $message }}</span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Username</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" style="color: black;" placeholder="Masukkan username" wire:model="username">
                                        <div>
                                            @error('username')
                                            <span class="text-danger">{{ $message }}</span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Password</label>
                                    <div class="col-sm-9">
                                        <input type="password" class="form-control" style="color: black;" placeholder="Masukkan password" wire:model="password">
                                        <div>
                                            @error('password')
                                            <span class="text-danger">{{ $message }}</span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Foto</label>
                                    <div class="col-sm-9">
                                        <input type="file" style="color: black;" wire:model="foto">
                                        <div>
                                            @error('foto')
                                            <span class="text-danger">{{ $message }}</span>
                                            @enderror
                                        </div>
                                        <div wire:loading wire:target="foto">Uploading...</div>
                                        @if ($foto)
                                        <div class="mt-3" style="object-fit: cover;">
                                            <div>Photo Preview</div>
                                            <img src="{{ $foto->temporaryUrl() }}" height="150" width="100">
                                        </div>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">KTP</label>
                                    <div class="col-sm-9">
                                        <input type="file" style="color: black;" wire:model="ktp">
                                        <div>
                                            @error('ktp')
                                            <span class="text-danger">{{ $message }}</span>
                                            @enderror
                                        </div>
                                        <div wire:loading wire:target="ktp">Uploading...</div>
                                        @if ($ktp)
                                        <div class="mt-3" style="object-fit: cover;">
                                            <div>Photo Preview</div>
                                            <img src="{{ $ktp->temporaryUrl() }}" height="100" width="150">
                                        </div>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Ijazah SD</label>
                                    <div class="col-sm-2">
                                        <input type="text" class="form-control" style="color: black;" wire:model="ket_sd" placeholder="Nama">
                                        <div>
                                            @error('ket_sd')
                                            <span class="text-danger">{{ $message }}</span>
                                            @enderror
                                        </div>
                                        <div wire:loading wire:target="sd">Uploading...</div>
                                        @if ($sd)
                                        <div class="mt-3" style="object-fit: cover;">
                                            <div>Photo Preview</div>
                                            <img src="{{ $sd->temporaryUrl() }}" height="150" width="100">
                                        </div>
                                        @endif
                                    </div>
                                    <div class="col-sm-2">
                                        <input type="text" class="form-control" style="color: black;" wire:model="tahun_sd1" placeholder="Tahun">
                                        <div>
                                            @error('tahun_sd1')
                                            <span class="text-danger">{{ $message }}</span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="d-flex align-content-center justify-content-center mt-3" style="color: black;">/</div>
                                    <div class="col-sm-2">
                                        <input type="text" class="form-control" style="color: black;" wire:model="tahun_sd2" placeholder="Tahun">
                                        <div>
                                            @error('tahun_sd2')
                                            <span class="text-danger">{{ $message }}</span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-sm-2">
                                        <input type="file" style="color: black;" wire:model="sd">
                                        <div>
                                            @error('sd')
                                            <span class="text-danger">{{ $message }}</span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Ijazah SMP</label>
                                    <div class="col-sm-2">
                                        <input type="text" class="form-control" style="color: black;" wire:model="ket_smp" placeholder="Nama">
                                        <div>
                                            @error('ket_smp')
                                            <span class="text-danger">{{ $message }}</span>
                                            @enderror
                                        </div>
                                        <div wire:loading wire:target="smp">Uploading...</div>
                                        @if ($smp)
                                        <div class="mt-3" style="object-fit: cover;">
                                            <div>Photo Preview</div>
                                            <img src="{{ $smp->temporaryUrl() }}" height="150" width="100">
                                        </div>
                                        @endif
                                    </div>
                                    <div class="col-sm-2">
                                        <input type="text" class="form-control" style="color: black;" wire:model="tahun_smp1" placeholder="Tahun">
                                        <div>
                                            @error('tahun_smp1')
                                            <span class="text-danger">{{ $message }}</span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="d-flex align-content-center justify-content-center mt-3" style="color: black;">/</div>
                                    <div class="col-sm-2">
                                        <input type="text" class="form-control" style="color: black;" wire:model="tahun_smp2" placeholder="Tahun">
                                        <div>
                                            @error('tahun_smp2')
                                            <span class="text-danger">{{ $message }}</span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-sm-2">
                                        <input type="file" style="color: black;" wire:model="smp">
                                        <div>
                                            @error('smp')
                                            <span class="text-danger">{{ $message }}</span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Ijazah SMA</label>
                                    <div class="col-sm-2">
                                        <input type="text" class="form-control" style="color: black;" wire:model="ket_sma" placeholder="Nama">
                                        <div>
                                            @error('ket_sma')
                                            <span class="text-danger">{{ $message }}</span>
                                            @enderror
                                        </div>
                                        <div>
                                            @error('sma')
                                            <span class="text-danger">{{ $message }}</span>
                                            @enderror
                                        </div>
                                        <div wire:loading wire:target="sma">Uploading...</div>
                                        @if ($sma)
                                        <div class="mt-3" style="object-fit: cover;">
                                            <div>Photo Preview</div>
                                            <img src="{{ $sma->temporaryUrl() }}" height="150" width="100">
                                        </div>
                                        @endif
                                    </div>
                                    <div class="col-sm-2">
                                        <input type="text" class="form-control" style="color: black;" wire:model="tahun_sma1" placeholder="Tahun">
                                        <div>
                                            @error('tahun_sma1')
                                            <span class="text-danger">{{ $message }}</span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="d-flex align-content-center justify-content-center mt-3" style="color: black;">/</div>
                                    <div class="col-sm-2">
                                        <input type="text" class="form-control" style="color: black;" wire:model="tahun_sma2" placeholder="Tahun">
                                        <div>
                                            @error('tahun_sma2')
                                            <span class="text-danger">{{ $message }}</span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-sm-2">
                                        <input type="file" style="color: black;" wire:model="sma">
                                        <div>
                                            @error('sma')
                                            <span class="text-danger">{{ $message }}</span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="d-flex justify-content-end">
                                    <button type="submit" class="btn btn-primary ml-3">Tambah</button>
                                </div>
                            </form>
                        </div>

                    </div>
                </div>
            </div>
        </div>

    </div>
</div>