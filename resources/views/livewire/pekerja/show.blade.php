<div class="content-body">

    <div class="container-fluid">
        <!-- Vectormap -->
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Pekerja</h4>
                    </div>
                    <div class="card-body">
                        <div class="dataTables_wrapper d-flex justify-content-between" wire:ignore>
                            <div class="dataTables_filter"><label>Search&nbsp;:&nbsp;<input type="search" wire:model="search"></label></div>
                            <div><label>Urutkan <select wire:model="urut" class="default-select">
                                        <option value="asc">Harga Terendah</option>
                                        <option value="desc">Harga Tertinggi</option>
                                    </select></label></div>
                        </div>
                        <!-- <div class="d-flex justify-content-end" wire:ignore>
                            <label>Kota <select wire:model="kota" class="default-select">
                                    <option value="">Filter kota</option>
                                    @foreach($kt as $k)
                                    <option value="{{$k->kota}}">{{$k->kota}}</option>
                                    @endforeach
                                </select></label>
                        </div> -->
                        <!-- Nav tabs -->
                        <div class="custom-tab-1">
                            <ul class="nav nav-tabs">
                                <li class="nav-item">
                                    <a class="nav-link active" data-toggle="tab" href="#art" wire:click="kategori('Asisten Rumah Tangga')" wire:ignore><i class="la la-user mr-2"></i> Asisten Rumah Tangga</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-toggle="tab" href="#sc" wire:click="kategori('Security')" wire:ignore><i class="la la-user mr-2"></i> Security</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-toggle="tab" href="#bs" wire:click="kategori('Baby Sitter')" wire:ignore><i class="la la-user mr-2"></i> Baby Sitter</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-toggle="tab" href="#pot" wire:click="kategori('Pengasuh Orang Tua')" wire:ignore><i class="la la-user mr-2"></i> Pengasuh Orang Tua</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-toggle="tab" href="#sp" wire:click="kategori('Sopir')" wire:ignore><i class="la la-user mr-2"></i> Sopir</a>
                                </li>
                            </ul>
                            <div class="tab-content">
                                <div class="row">
                                    @foreach($pekerja as $p)
                                    <div class="col-lg-3">
                                        <div class="card mb-3">
                                            <div class="card-header text-center">
                                                <h5 class="card-title mx-auto"><a href="/pekerja/detail/{{$p->user->username}}">{{$p->nama ?? 'Pekerja'}}</a></h5>
                                            </div>
                                            <div class="card-body">
                                                <a href="/pekerja/detail/{{$p->user->username}}"><img class="card-img-bottom" src="/storage/{{$p->foto}}" width="100px" height="300px"></a>
                                            </div>
                                            <div class="card-footer">
                                                <h5 class="text-center">Kota {{$p->kota}}</h5>
                                                <h5 class="text-center">Rp {{$p->harga}}</h5>
                                            </div>
                                        </div>
                                    </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

</div>