<div class="content-body">

    <div class="container-fluid">

        <div class="row gutters-sm">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-sm-4">
                                <div class="d-flex flex-column align-items-center text-center">
                                    <a href="/storage/{{$pekerja->foto}}" data-lightbox="mygallery"><img src="/storage/{{$pekerja->foto}}" alt="Admin" width="200" height="250"></a>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="row">
                                    <div class="col-sm-5">
                                        <h6>Nama</h6>
                                    </div>
                                    <div class="col-sm-1">
                                        <h6>:</h6>
                                    </div>
                                    <div class="col-sm-6">
                                        <h6>{{$pekerja->nama}}</h6>
                                    </div>
                                </div>
                                <hr>
                                <div class="row">
                                    <div class="col-sm-5">
                                        <h6>Jenis Kelamin</h6>
                                    </div>
                                    <div class="col-sm-1">
                                        <h6>:</h6>
                                    </div>
                                    <div class="col-sm-6">
                                        <h6>{{$pekerja->jenis_kelamin}}</h6>
                                    </div>
                                </div>
                                <hr>
                                <div class="row">
                                    <div class="col-sm-5">
                                        <h6>Tanggal Lahir</h6>
                                    </div>
                                    <div class="col-sm-1">
                                        <h6>:</h6>
                                    </div>
                                    <div class="col-sm-6">
                                        <h6>{{$pekerja->tanggal_lahir->format('d, F Y')}}</h6>
                                    </div>
                                </div>
                                <hr>
                                <div class="row">
                                    <div class="col-sm-5">
                                        <h6>Umur</h6>
                                    </div>
                                    <div class="col-sm-1">
                                        <h6>:</h6>
                                    </div>
                                    <div class="col-sm-6">
                                        <h6>{{$pekerja->umur}}</h6>
                                    </div>
                                </div>
                                <hr>
                                <div class="row">
                                    <div class="col-sm-5">
                                        <h6>No HP</h6>
                                    </div>
                                    <div class="col-sm-1">
                                        <h6>:</h6>
                                    </div>
                                    <div class="col-sm-6">
                                        <h6>{{$pekerja->no_hp}}</h6>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="row">
                                    <div class="col-sm-5">
                                        <h6>Email</h6>
                                    </div>
                                    <div class="col-sm-1">
                                        <h6>:</h6>
                                    </div>
                                    <div class="col-sm-6">
                                        <h6>{{$pekerja->email}}</h6>
                                    </div>
                                </div>
                                <hr>
                                <div class="row">
                                    <div class="col-sm-5">
                                        <h6>Alamat</h6>
                                    </div>
                                    <div class="col-sm-1">
                                        <h6>:</h6>
                                    </div>
                                    <div class="col-sm-6">
                                        <h6>{{$pekerja->alamat}}</h6>
                                    </div>
                                </div>
                                <hr>
                                <div class="row">
                                    <div class="col-sm-5">
                                        <h6>Kota</h6>
                                    </div>
                                    <div class="col-sm-1">
                                        <h6>:</h6>
                                    </div>
                                    <div class="col-sm-6">
                                        <h6>{{$pekerja->kota}}</h6>
                                    </div>
                                </div>
                                <hr>
                                <div class="row">
                                    <div class="col-sm-5">
                                        <h6>Jenis</h6>
                                    </div>
                                    <div class="col-sm-1">
                                        <h6>:</h6>
                                    </div>
                                    <div class="col-sm-6">
                                        <h6>{{$pekerja->jenis}}</h6>
                                    </div>
                                </div>
                                <hr>
                                <div class="row">
                                    <div class="col-sm-5">
                                        <h6>Harga</h6>
                                    </div>
                                    <div class="col-sm-1">
                                        <h6>:</h6>
                                    </div>
                                    <div class="col-sm-6">
                                        <h6>Rp {{$pekerja->harga}} / bulan</h6>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <h3 class="mt-5">Riwayat Pendidikan</h3>
                                    </div>
                                </div>
                                @if($pekerja->sd != null)
                                <div class="row mb-3">
                                    <div class="col-sm-9">
                                        <h6>{{$sd[0]}} Tahun {{$sd[1]}}</h6>
                                    </div>
                                    <div class="col-sm-3">
                                        <a href="/storage/{{$pekerja->sd}}" data-lightbox="mygallery"><img src="/storage/{{$pekerja->sd}}" height="80" width="50"></a>
                                    </div>
                                </div>
                                @endif
                                @if($pekerja->smp != null)
                                <div class="row mb-3">
                                    <div class="col-sm-9">
                                        <h6>{{$smp[0]}} Tahun {{$smp[1]}}</h6>
                                    </div>
                                    <div class="col-sm-3">
                                        <a href="/storage/{{$pekerja->smp}}" data-lightbox="mygallery"><img src="/storage/{{$pekerja->smp}}" height="80" width="50"></a>
                                    </div>
                                </div>
                                @endif
                                @if($pekerja->sma != null)
                                <div class="row mb-3">
                                    <div class="col-sm-9">
                                        <h6>{{$sma[0]}} Tahun {{$sma[1]}}</h6>
                                    </div>
                                    <div class="col-sm-3">
                                        <a href="/storage/{{$pekerja->sma}}" data-lightbox="mygallery"><img src="/storage/{{$pekerja->sma}}" height="80" width="50"></a>
                                    </div>
                                </div>
                                @endif
                            </div>
                            <div class="col-lg-6">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <h3 class="mt-5">Dokumen Pendukung</h3>
                                    </div>
                                </div>

                                <div class="row mb-3">
                                    <div class="col-sm-9">
                                        <h6>KTP</h6>
                                    </div>
                                    <div class="col-sm-3">
                                        <a href="/storage/{{$pekerja->dokumen}}" data-lightbox="mygallery"><img src="/storage/{{$pekerja->dokumen}}" height="50" width="80"></a>
                                    </div>
                                </div>

                                @foreach($ser as $s)
                                <div class="row mb-3">
                                    <div class="col-sm-9">
                                        <h6>{{$s->keterangan}}</h6>
                                    </div>
                                    <div class="col-sm-3">
                                        <a href="/storage/{{$s->sertifikat}}" data-lightbox="mygallery"><img src="/storage/{{$s->sertifikat}}" height="50" width="80"></a>
                                    </div>
                                </div>
                                @endforeach

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>