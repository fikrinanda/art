<div class="content-body">
    <div class="container-fluid">
        <div class="form-head d-flex mb-3 align-items-start">
            <div class="mr-auto">
                <h2 class="text-black font-w600 mb-0">Dashboard Pekerja</h2>
                <p class="mb-0">Selamat Datang!</p>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Rating Sistem Informasi</h4>
                        @if(!$test && auth()->user()->level != 'Admin')
                        <div class="d-sm-flex align-items-center">
                            <a href="/rating/tambah" class="btn btn-outline-primary rounded">Tambah</a>
                        </div>
                        @endif
                    </div>
                    <div class="card-body">
                        <div class="dataTables_wrapper d-flex justify-content-between" wire:ignore>
                            <div class="dataTables_length"><label>Show <select wire:model="perPage" class="default-select">
                                        <option value="5">5</option>
                                        <option value="10">10</option>
                                        <option value="15">15</option>
                                        <option value="20">20</option>
                                    </select></label></div>
                            <div class="dataTables_filter"><label>Search&nbsp;:&nbsp;<input type="search" wire:model="search"></label></div>
                        </div>
                        @if(count($rating) > 0)
                        <div class="table-responsive">
                            <table class="table table-responsive-md">
                                <thead>
                                    <tr>
                                        <th class="width80">No</th>
                                        <th>Username</th>
                                        <th>Keterangan</th>
                                        <th>Rating</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($rating as $key => $a)
                                    <tr>
                                        <td style="width: 20%;"><strong>{{$rating->firstItem() + $key}}</strong></td>
                                        <td style="width: 20%;">{{$a->user->username}}</td>
                                        <td style="width: 20%;">{{$a->keterangan}}</td>
                                        <td style="width: 20%;">{{$a->rating}}</td>
                                        <td>
                                            <div id="rating{{$rating->firstItem()+$key}}" data-score="{{$a->rating}}">
                                            </div>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        <hr>
                        <h2>Rata - Rata Rating : {{$avg}}</h2>
                        <div class="mt-3">
                            {{$rating->links()}}
                        </div>
                        @else
                        <h1>Tidak ada data</h1>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@section('js')
<link rel="stylesheet" href="{{asset('scripts/lib/jquery.raty.css')}}">
</link>
<script src="{{asset('scripts/lib/jquery.raty.js')}}"></script>
<script>
    $("tr").each(function(index) {
        $('#rating' + index).raty({
            readOnly: true
        });
    });
</script>
@endsection