<div wire:poll>
    @if($angka>0)
    <span class="badge light text-white bg-primary" wire:poll>{{$angka}}</span>
    @endif
    @if(auth()->user()->pekerja->nama == null)
    <span class="badge light text-white bg-primary" wire:poll>1</span>
    @endif
</div>