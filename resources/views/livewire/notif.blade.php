<div class="dropdown-menu dropdown-menu-right">
    <div id="DZ_W_Notification1" class="widget-media dz-scroll p-3 height380">
        <ul class="timeline">
            @if(auth()->user()->pekerja->nama == null)
            <li>
                <div class="timeline-panel">
                    <div class="media mr-2 media-primary">
                        <i class="fa fa-info"></i>
                    </div>
                    <div class="media-body">
                        <a href="/profil/ubah">
                            <h6 class="mb-1">Lengkapi profil anda untuk memenuhi persyaratan</h6>
                        </a>
                        <small class="d-block">{{ date('Y-m-d H:i:s') }}</small>
                    </div>
                </div>
            </li>
            @endif
            @if(count($pelanggan) > 0)
            @foreach($pelanggan as $p)
            @if($p->status == 'Pending')
            <li>
                <div class="timeline-panel">
                    <div class="media mr-2 media-primary">
                        <i class="fa fa-user"></i>
                    </div>
                    <div class="media-body" wire:click="halo">
                        <a href="/pesan/konfirmasi/{{$p->id}}">

                            <h6 class="mb-1">{{$p->pelanggan->nama}} telah mengajukan negosiasi dengan anda, jika konfirmasi lanjut chat</h6>


                        </a>
                        <small class="d-block">{{$p->updated_at->format('d F Y - H:i:s')}}</small>
                    </div>
                </div>
            </li>
            @elseif($p->status == 'Chat')
            <li>
                <div class="timeline-panel">
                    <div class="media mr-2 media-primary">
                        <i class="fa fa-user"></i>
                    </div>
                    <div class="media-body" wire:click="halo">


                        <h6 class="mb-1">Anda telah menerima ajuan negosiasi {{$p->pelanggan->nama}}. Lanjut chat untuk menghubungi pelanggan</h6>



                        <small class="d-block">{{$p->updated_at->format('d F Y - H:i:s')}}</small>
                    </div>
                </div>
            </li>
            @elseif($p->status == 'Ajukan')
            <li>
                <div class="timeline-panel">
                    <div class="media mr-2 media-primary">
                        <i class="fa fa-user"></i>
                    </div>
                    <div class="media-body" wire:click="halo">
                        <a href="/pesan/konfirmasi/{{$p->id}}">

                            <h6 class="mb-1">{{$p->pelanggan->nama}} telah memesan anda, klik untuk konfirmasi</h6>


                        </a>
                        <small class="d-block">{{$p->updated_at->format('d F Y - H:i:s')}}</small>
                    </div>
                </div>
            </li>
            @elseif($p->status == 'Tolak')
            <li>
                <div class="timeline-panel">
                    <div class="media mr-2 media-primary">
                        <i class="fa fa-user"></i>
                    </div>
                    <div class="media-body" wire:click="halo">

                        <h6 class="mb-1">Anda telah menolak pesanan dengan {{$p->pelanggan->nama}}</h6>



                        <small class="d-block">{{$p->updated_at->format('d F Y - H:i:s')}}</small>
                    </div>
                </div>
            </li>
            @elseif($p->status == 'Konfirmasi')
            <li>
                <div class="timeline-panel">
                    <div class="media mr-2 media-primary">
                        <i class="fa fa-user"></i>
                    </div>
                    <div class="media-body" wire:click="halo">
                        <a href="/pesan/konfirmasi/{{$p->id}}">

                            <h6 class="mb-1">Anda telah setuju dengan pelanggan {{$p->pelanggan->nama}}</h6>


                        </a>
                        <small class="d-block">{{$p->updated_at->format('d F Y - H:i:s')}}</small>
                    </div>
                </div>
            </li>
            @elseif($p->status == 'Batal')
            <li>
                <div class="timeline-panel">
                    <div class="media mr-2 media-primary">
                        <i class="fa fa-user"></i>
                    </div>
                    <div class="media-body" wire:click="halo">
                        <a href="#">

                            <h6 class="mb-1">Pesanan anda dengan Pelanggan {{$p->pelanggan->nama}} telah dibatalkan</h6>

                        </a>

                        <small class="d-block">{{$p->updated_at->format('d F Y - H:i:s')}}</small>
                    </div>
                </div>
            </li>
            @endif
            @endforeach
            @endif
        </ul>
    </div>
    <!-- <a class="all-notification" href="javascript:void(0)">See all notifications <i class="ti-arrow-right"></i></a> -->
</div>