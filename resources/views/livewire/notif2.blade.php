<div class="dropdown-menu dropdown-menu-right">
    <div id="DZ_W_Notification1" class="widget-media dz-scroll p-3 height380">
        <ul class="timeline">

            @if(count($pelanggan) > 0)
            @foreach($pelanggan as $p)
            @if($p->status == 'Pending')
            <li>
                <div class="timeline-panel">
                    <div class="media mr-2 media-primary">
                        <i class="fa fa-user"></i>
                    </div>
                    <div class="media-body" wire:click="halo">

                        <a href="/pekerja/detail/{{$p->pekerja->user->username}}">
                            <h6 class="mb-1">Anda telah mengajukan negosiasi dengan pekerja {{$p->pekerja->nama}} , tunggu pekerja konfirmasi</h6>
                        </a>

                        <small class="d-block">{{$p->updated_at->format('d F Y - H:i:s')}}</small>
                    </div>
                </div>
            </li>
            @elseif($p->status == 'Chat')
            <li>
                <div class="timeline-panel">
                    <div class="media mr-2 media-primary">
                        <i class="fa fa-user"></i>
                    </div>
                    <div class="media-body" wire:click="halo">


                        <h6 class="mb-1">Pekerja {{$p->pekerja->nama}} telah menerima negosiasi dengan anda. Lanjut chat untuk menghubungi pekerja</h6>



                        <small class="d-block">{{$p->updated_at->format('d F Y - H:i:s')}}</small>
                    </div>
                </div>
            </li>
            @elseif($p->status == 'Ajukan')
            <li>
                <div class="timeline-panel">
                    <div class="media mr-2 media-primary">
                        <i class="fa fa-user"></i>
                    </div>
                    <div class="media-body" wire:click="halo">
                        <a href="/pekerja/detail/{{$p->pekerja->user->username}}">


                            <h6 class="mb-1">Anda telah memesan pekerja {{$p->pekerja->nama}} , tunggu pekerja konfirmasi</h6>

                        </a>


                        <small class="d-block">{{$p->updated_at->format('d F Y - H:i:s')}}</small>
                    </div>
                </div>
            </li>
            @elseif($p->status == 'Tolak')
            <li>
                <div class="timeline-panel">
                    <div class="media mr-2 media-primary">
                        <i class="fa fa-user"></i>
                    </div>
                    <div class="media-body" wire:click="halo">
                        <a href="/pekerja/detail/{{$p->pekerja->user->username}}">

                            <h6 class="mb-1">Pekerja {{$p->pekerja->nama}} telah menolak pesan anda</h6>

                        </a>

                        <small class="d-block">{{$p->updated_at->format('d F Y - H:i:s')}}</small>
                    </div>
                </div>
            </li>
            @elseif($p->status == 'Konfirmasi')
            <li>
                <div class="timeline-panel">
                    <div class="media mr-2 media-primary">
                        <i class="fa fa-user"></i>
                    </div>
                    <div class="media-body" wire:click="halo">
                        <a href="/pekerja/detail/{{$p->pekerja->user->username}}">

                            <h6 class="mb-1">Pekerja {{$p->pekerja->nama}} telah setuju dengan anda anda</h6>

                        </a>

                        <small class="d-block">{{$p->updated_at->format('d F Y - H:i:s')}}</small>
                    </div>
                </div>
            </li>
            @elseif($p->status == 'Batal')
            <li>
                <div class="timeline-panel">
                    <div class="media mr-2 media-primary">
                        <i class="fa fa-user"></i>
                    </div>
                    <div class="media-body" wire:click="halo">
                        <a href="/pekerja/detail/{{$p->pekerja->user->username}}">

                            <h6 class="mb-1">Pesanan anda dengan Pekerja {{$p->pekerja->nama}} telah dibatalkan</h6>

                        </a>

                        <small class="d-block">{{$p->updated_at->format('d F Y - H:i:s')}}</small>
                    </div>
                </div>
            </li>
            @endif
            @endforeach
            @endif
        </ul>
    </div>
    <!-- <a class="all-notification" href="javascript:void(0)">See all notifications <i class="ti-arrow-right"></i></a> -->
</div>