<div class="content-body">
    <div class="container-fluid">
        <!-- Vectormap -->
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Data Admin</h4>
                        <div class="d-sm-flex align-items-center">
                            <a href="/admin/tambah" class="btn btn-outline-primary rounded">Tambah</a>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="dataTables_wrapper d-flex justify-content-between" wire:ignore>
                            <div class="dataTables_length"><label>Show <select wire:model="perPage" class="default-select">
                                        <option value="5">5</option>
                                        <option value="10">10</option>
                                        <option value="15">15</option>
                                        <option value="20">20</option>
                                    </select></label></div>
                            <div class="dataTables_filter"><label>Search&nbsp;:&nbsp;<input type="search" wire:model="search"></label></div>
                        </div>
                        @if(count($admin) > 0)
                        <div class="table-responsive">
                            <table class="table table-responsive-md">
                                <thead>
                                    <tr>
                                        <th class="width80">No</th>
                                        <th wire:click="sortBy('id')" style="cursor: pointer;">
                                            ID
                                            @include('partials._sort-icon',['field'=>'id'])
                                        </th>
                                        <th wire:click="sortBy('username')" style="cursor: pointer;">
                                            Username
                                            @include('partials._sort-icon',['field'=>'username'])
                                        </th>
                                        <th>Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($admin as $key => $a)
                                    <tr>
                                        <td style="width: 20%;"><strong>{{$admin->firstItem() + $key}}</strong></td>
                                        <td style="width: 20%;">{{$a->id}}</td>
                                        <td style="width: 20%;">{{$a->username}}</td>
                                        <td style="width: 20%;">
                                            <div class="d-flex">
                                                <a href="/admin/ubah/{{$a->username}}" class="btn btn-warning shadow sharp mr-1" data-toggle="tooltip" data-placement="top" title="Ubah"><i class="fa fa-edit"></i></a>
                                                @if($a->id != auth()->user()->id)
                                                <button wire:click="hapus('{{$a->id}}')" class="btn btn-danger shadow sharp mr-1" data-toggle="tooltip" data-placement="top" title="Hapus"><i class="fa fa-trash"></i></button>
                                                @endif
                                            </div>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        <div class="mt-3">
                            {{$admin->links()}}
                        </div>
                        @else
                        <h1>Tidak ada data</h1>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>