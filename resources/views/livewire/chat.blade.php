<div class="chatbox" wire:ignore.self>
    <div class="chatbox-close"></div>
    <div class="custom-tab-1">
        <ul class="nav nav-tabs">
            <li class="nav-item">
                <a class="nav-link" data-toggle="tab" href="#"></a>
            </li>
            <li class="nav-item">
                <a class="nav-link active" data-toggle="tab" href="#">Chat</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" data-toggle="tab" href="#"></a>
            </li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane fade active show" id="chat" role="tabpanel">
                <div class="card mb-sm-3 mb-md-0 contacts_card dz-chat-user-box" wire:ignore.self>
                    <div class="card-header chat-list-header text-center">
                        <div>
                            <h6 class="mb-1"></h6>
                            <p class="mb-0"></p>
                        </div>
                    </div>
                    <div class="card-body contacts_body p-0 dz-scroll" id="DZ_W_Contacts_Body">
                        <ul class="contacts" wire:poll>
                            @if(auth()->user()->level=='Pelanggan')
                            @foreach($user as $key => $u)
                            @if(App\Models\Pesan::where('pekerja_id', $u->pekerja->id)->where(function($q){
                            $q->where('status', 'Chat');
                            $q->orWhere('status', 'Ajukan');
                            $q->orWhere('status', 'Pending');
                            })->exists())
                            <li class="active dz-chat-user" wire:click="coba('{{$u->id}}')" wire:poll>
                                <div class="d-flex bd-highlight">
                                    <div class="img_cont">
                                        <img src="/storage/{{$u->pekerja->foto}}" width="20" height="40" />
                                        <!-- <span class="online_icon"></span> -->
                                    </div>
                                    <div class="user_info">
                                        <span>{{$u->pekerja->nama}}</span>
                                        <p>{{$f[$key]}}</p>
                                    </div>
                                    <div class="ml-auto h5">
                                        @if(App\Models\Obrolan::where('pengirim', $u->id)->where('is_pelanggan', 'Belum')->count() != 0)
                                        {{App\Models\Obrolan::where('pengirim', $u->id)->where('is_pelanggan', 'Belum')->count()}}
                                        @endif
                                    </div>
                                </div>
                            </li>
                            @endif
                            @endforeach
                            @elseif(auth()->user()->level=='Pekerja')
                            @foreach($user as $key => $u)
                            @if(App\Models\Pesan::where('pelanggan_id', $u->pelanggan->id)->where(function($q){
                            $q->where('status', 'Chat');
                            $q->orWhere('status', 'Ajukan');
                            $q->orWhere('status', 'Pending');
                            })->exists())
                            <li class="active dz-chat-user" wire:click="coba('{{$u->id}}')" wire:poll>
                                <div class="d-flex bd-highlight">
                                    <div class="img_cont">
                                        <img src="/storage/{{$u->pelanggan->foto}}" width="20" height="40" />
                                        <!-- <span class="online_icon"></span> -->
                                    </div>
                                    <div class="user_info">
                                        <span>{{$u->pelanggan->nama}}</span>
                                        <p>{{$f[$key]}}</p>
                                    </div>
                                    <div class="ml-auto h5">
                                        @if(App\Models\Obrolan::where('pengirim', $u->id)->where('is_pekerja', 'Belum')->count() != 0)
                                        {{App\Models\Obrolan::where('pengirim', $u->id)->where('is_pekerja', 'Belum')->count()}}
                                        @endif
                                    </div>
                                </div>
                            </li>
                            @endif
                            @endforeach
                            @endif
                        </ul>
                    </div>
                </div>
                <div class="card chat dz-chat-history-box" wire:ignore.self>
                    <div class="card-header chat-list-header text-center">
                        <a href="javascript:void(0)" class="dz-chat-history-back">
                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="18px" height="18px" viewBox="0 0 24 24" version="1.1">
                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                    <polygon points="0 0 24 0 24 24 0 24" />
                                    <rect fill="#000000" opacity="0.3" transform="translate(15.000000, 12.000000) scale(-1, 1) rotate(-90.000000) translate(-15.000000, -12.000000) " x="14" y="7" width="2" height="10" rx="1" />
                                    <path d="M3.7071045,15.7071045 C3.3165802,16.0976288 2.68341522,16.0976288 2.29289093,15.7071045 C1.90236664,15.3165802 1.90236664,14.6834152 2.29289093,14.2928909 L8.29289093,8.29289093 C8.67146987,7.914312 9.28105631,7.90106637 9.67572234,8.26284357 L15.6757223,13.7628436 C16.0828413,14.136036 16.1103443,14.7686034 15.7371519,15.1757223 C15.3639594,15.5828413 14.7313921,15.6103443 14.3242731,15.2371519 L9.03007346,10.3841355 L3.7071045,15.7071045 Z" fill="#000000" fill-rule="nonzero" transform="translate(9.000001, 11.999997) scale(-1, -1) rotate(90.000000) translate(-9.000001, -11.999997) " />
                                </g>
                            </svg>
                        </a>
                        <div class="d-flex justify-content-center align-items-center">
                            <h6 class="mb-1" style="margin-left: 85px;">{{$i}}</h6>
                            <button type="button" wire:click="hapus" class="ml-5 btn btn-primary btn-sm" style="border-radius: 15%;">Batal</button>
                            <!-- <p class="mb-0 text-success">Online</p> -->

                        </div>


                        <div class="dropdown" wire:ignore>

                        </div>
                    </div>
                    <div class="card-body msg_card_body dz-scroll" id="DZ_W_Contacts_Body3" wire:ignore.self wire:poll>
                        @foreach($ob as $o)
                        @if($o->pengirim == auth()->user()->id)
                        <div class="d-flex justify-content-end mb-4" wire:poll>
                            <div class="msg_cotainer_send">
                                {{$o->pesan}}
                                <span class="msg_time_send">{{$o->created_at->format('d F Y - H:i:s')}}</span>
                            </div>
                            <div class="img_cont_msg">
                                @if(auth()->user()->level == 'Pelanggan')
                                <img src="/storage/{{auth()->user()->pelanggan->foto}}" width="20" height="40" />
                                @elseif(auth()->user()->level == 'Pekerja')
                                <img src="/storage/{{auth()->user()->pekerja->foto}}" width="20" height="40" />
                                @endif
                            </div>
                        </div>
                        @else
                        <div class="d-flex justify-content-start mb-4" wire:poll>
                            <div class="img_cont_msg">
                                @php
                                $us = App\Models\User::find($i2);
                                @endphp
                                @if($us->level == 'Pelanggan')
                                <img src="/storage/{{$us->pelanggan->foto}}" width="20" height="40" />
                                @elseif($us->level == 'Pekerja')
                                <img src="/storage/{{$us->pekerja->foto}}" width="20" height="40" />
                                @endif
                            </div>
                            <div class="msg_cotainer">
                                {{$o->pesan}}
                                <span class="msg_time">{{$o->created_at->format('d F Y - H:i:s')}}</span>
                            </div>
                        </div>
                        @endif
                        @endforeach


                    </div>
                    @if(auth()->user()->level == "Pelanggan" && App\Models\Pesan::where('pelanggan_id', auth()->user()->pelanggan->id)->where('pekerja_id', $i3)->where(function($q){
                    $q->where('status', 'Chat');
                    $q->orWhere('status', 'Ajukan');
                    })->exists())
                    <div class="card-footer type_msg">
                        <form wire:submit.prevent="kirim" autocomplete="off">
                            <div class="input-group">
                                <textarea class="form-control" placeholder="Type your message..." wire:model="pesan"></textarea>
                                <div class="input-group-append">
                                    <button type="submit" class="btn btn-primary"><i class="fa fa-location-arrow"></i></button>
                                    @if(auth()->user()->level == 'Pelanggan')
                                    <button type="button" wire:click="pesan2" class="btn btn-primary btn-sm ml-2 d-flex justify-content-center align-items-center">
                                        <div style="font-size: 12px;">Pesan</div>
                                    </button>
                                    @endif
                                </div>
                            </div>
                        </form>
                    </div>
                    @elseif(auth()->user()->level == "Pekerja" && App\Models\Pesan::where('pelanggan_id', $i3)->where('pekerja_id', auth()->user()->pekerja->id)->where(function($q){
                    $q->where('status', 'Chat');
                    $q->orWhere('status', 'Ajukan');
                    })->exists())
                    <div class="card-footer type_msg">
                        <form wire:submit.prevent="kirim" autocomplete="off">
                            <div class="input-group">
                                <textarea class="form-control" placeholder="Type your message..." wire:model="pesan"></textarea>
                                <div class="input-group-append">
                                    <button type="submit" class="btn btn-primary"><i class="fa fa-location-arrow"></i></button>
                                    @if(auth()->user()->level == 'Pelanggan')
                                    <button type="button" wire:click="pesan2" class="btn btn-primary btn-sm ml-2 d-flex justify-content-center align-items-center">
                                        <div style="font-size: 12px;">Pesan</div>
                                    </button>
                                    @endif
                                </div>
                            </div>
                        </form>
                    </div>
                    @endif
                </div>

            </div>

        </div>
    </div>
</div>