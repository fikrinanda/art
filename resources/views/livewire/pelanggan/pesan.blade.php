<div class="content-body">
    <div class="container-fluid">
        <div class="row gutters-sm">
            <div class="col-lg-12">
                <div class="card">

                    <div class="card-body">
                        <div class="d-flex flex-column align-items-center text-center">
                            <h4 class="card-title">Pesan Pekerja</h4>
                            <img src="/storage/{{$pekerja->foto}}" alt="Admin" width="300" height="350">
                            <div class="mt-3">
                                <h4>{{$pekerja->nama}}</h4>
                            </div>
                        </div>
                        <br>
                        <div class="basic-form">
                            <form wire:submit.prevent="pesan" autocomplete="off">
                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Mulai Kerja</label>
                                    <div class="col-sm-3">
                                        <input type="date" class="form-control" style="color: black;" wire:model="mulai">
                                        <div>
                                            @error('mulai')
                                            <span class="text-danger">{{ $message }}</span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="d-flex justify-content-end">
                                    <button type="submit" class="btn btn-primary ml-3">Pesan</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

            </div>
        </div>

    </div>
</div>