<div class="content-body">
    <div class="container-fluid">
        <div class="row">
            <div class="col-xl-6 col-lg-6">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">Tambah Pelanggan</h4>
                            </div>
                            <div class="card-body">
                                <div class="basic-form">
                                    <form wire:submit.prevent="tambah" autocomplete="off">
                                        <div class="form-group row">
                                            <label class="col-sm-3 col-form-label">Nama</label>
                                            <div class="col-sm-9">
                                                <input type="text" class="form-control" style="color: black;" placeholder="Masukkan nama" wire:model="nama">
                                                <div>
                                                    @error('nama')
                                                    <span class="text-danger">{{ $message }}</span>
                                                    @enderror
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-sm-3 col-form-label">Jenis Kelamin</label>
                                            <div class="col-sm-9 mt-2">
                                                <label class="radio-inline mr-3"><input type="radio" wire:model="jenis_kelamin" value="Laki-laki"> Laki-laki</label>
                                                <label class="radio-inline mr-3"><input type="radio" wire:model="jenis_kelamin" value="Perempuan"> Perempuan</label>
                                                <div>
                                                    @error('jenis_kelamin')
                                                    <span class="text-danger">{{ $message }}</span>
                                                    @enderror
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-sm-3 col-form-label">Tanggal Lahir</label>
                                            <div class="col-sm-9">
                                                <input type="date" class="form-control" style="color: black;" wire:model="tanggal_lahir" max="{{$tgl}}">
                                                <div>
                                                    @error('tanggal_lahir')
                                                    <span class="text-danger">{{ $message }}</span>
                                                    @enderror
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-sm-3 col-form-label">Alamat</label>
                                            <div class="col-sm-9">
                                                <textarea class="form-control" rows="4" wire:model="alamat" style="color: black; resize: none;" placeholder="Masukkan alamat"></textarea>
                                                <div>
                                                    @error('alamat')
                                                    <span class="text-danger">{{ $message }}</span>
                                                    @enderror
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-sm-3 col-form-label">Kota</label>
                                            <div class="col-sm-9">
                                                <input type="text" class="form-control" style="color: black;" placeholder="Masukkan kota" wire:model="kota">
                                                <div>
                                                    @error('kota')
                                                    <span class="text-danger">{{ $message }}</span>
                                                    @enderror
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-sm-3 col-form-label">Email</label>
                                            <div class="col-sm-9">
                                                <input type="text" class="form-control" style="color: black;" placeholder="Masukkan email" wire:model="email">
                                                <div>
                                                    @error('email')
                                                    <span class="text-danger">{{ $message }}</span>
                                                    @enderror
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-sm-3 col-form-label">No HP</label>
                                            <div class="col-sm-9">
                                                <input type="text" class="form-control" style="color: black;" placeholder="Masukkan no HP" wire:model="no_hp">
                                                <div>
                                                    @error('no_hp')
                                                    <span class="text-danger">{{ $message }}</span>
                                                    @enderror
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-sm-3 col-form-label">Username</label>
                                            <div class="col-sm-9">
                                                <input type="text" class="form-control" style="color: black;" placeholder="Masukkan username" wire:model="username">
                                                <div>
                                                    @error('username')
                                                    <span class="text-danger">{{ $message }}</span>
                                                    @enderror
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-sm-3 col-form-label">Password</label>
                                            <div class="col-sm-9">
                                                <input type="password" class="form-control" style="color: black;" placeholder="Masukkan password" wire:model="password">
                                                <div>
                                                    @error('password')
                                                    <span class="text-danger">{{ $message }}</span>
                                                    @enderror
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-sm-3 col-form-label">Foto</label>
                                            <div class="col-sm-9">
                                                <input type="file" style="color: black;" wire:model="foto">
                                                <div>
                                                    @error('foto')
                                                    <span class="text-danger">{{ $message }}</span>
                                                    @enderror
                                                </div>
                                                <div wire:loading wire:target="foto">Uploading...</div>
                                                @if ($foto)
                                                <div class="mt-3" style="object-fit: cover;">
                                                    <div>Photo Preview:</div>
                                                    <img src="{{ $foto->temporaryUrl() }}" height="150" width="100">
                                                </div>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-sm-3 col-form-label">KTP</label>
                                            <div class="col-sm-9">
                                                <input type="file" style="color: black;" wire:model="ktp">
                                                <div>
                                                    @error('ktp')
                                                    <span class="text-danger">{{ $message }}</span>
                                                    @enderror
                                                </div>
                                                <div wire:loading wire:target="ktp">Uploading...</div>
                                                @if ($ktp)
                                                <div class="mt-3" style="object-fit: cover;">
                                                    <div>Photo Preview:</div>
                                                    <img src="{{ $ktp->temporaryUrl() }}" height="100" width="150">
                                                </div>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="d-flex justify-content-end">
                                            <button type="submit" class="btn btn-primary ml-3">Tambah</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>