<div class="content-body">

    <div class="container-fluid">

        <div class="row gutters-sm">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-sm-4">
                                <div class="d-flex flex-column align-items-center text-center">
                                    <a href="/storage/{{$pelanggan->foto}}" data-lightbox="mygallery"><img src="/storage/{{$pelanggan->foto}}" alt="Admin" width="200" height="250"></a>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="row">
                                    <div class="col-sm-5">
                                        <h6>Nama</h6>
                                    </div>
                                    <div class="col-sm-1">
                                        <h6>:</h6>
                                    </div>
                                    <div class="col-sm-6">
                                        <h6>{{$pelanggan->nama}}</h6>
                                    </div>
                                </div>
                                <hr>
                                <div class="row">
                                    <div class="col-sm-5">
                                        <h6>Jenis Kelamin</h6>
                                    </div>
                                    <div class="col-sm-1">
                                        <h6>:</h6>
                                    </div>
                                    <div class="col-sm-6">
                                        <h6>{{$pelanggan->jenis_kelamin}}</h6>
                                    </div>
                                </div>
                                <hr>
                                <div class="row">
                                    <div class="col-sm-5">
                                        <h6>Tanggal Lahir</h6>
                                    </div>
                                    <div class="col-sm-1">
                                        <h6>:</h6>
                                    </div>
                                    <div class="col-sm-6">
                                        <h6>{{$pelanggan->tanggal_lahir->format('d, F Y')}}</h6>
                                    </div>
                                </div>
                                <hr>
                                <div class="row">
                                    <div class="col-sm-5">
                                        <h6>Email</h6>
                                    </div>
                                    <div class="col-sm-1">
                                        <h6>:</h6>
                                    </div>
                                    <div class="col-sm-6">
                                        <h6>{{$pelanggan->email}}</h6>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="row">
                                    <div class="col-sm-5">
                                        <h6>Telepon</h6>
                                    </div>
                                    <div class="col-sm-1">
                                        <h6>:</h6>
                                    </div>
                                    <div class="col-sm-6">
                                        <h6>{{$pelanggan->no_hp}}</h6>
                                    </div>
                                </div>
                                <hr>
                                <div class="row">
                                    <div class="col-sm-5">
                                        <h6>Alamat</h6>
                                    </div>
                                    <div class="col-sm-1">
                                        <h6>:</h6>
                                    </div>
                                    <div class="col-sm-6">
                                        <h6>{{$pelanggan->alamat}}</h6>
                                    </div>
                                </div>
                                <hr>
                                <div class="row">
                                    <div class="col-sm-5">
                                        <h6>Kota</h6>
                                    </div>
                                    <div class="col-sm-1">
                                        <h6>:</h6>
                                    </div>
                                    <div class="col-sm-6">
                                        <h6>{{$pelanggan->kota}}</h6>
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-6">
                            </div>
                            <div class="col-lg-6">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <h3 class="mt-5">Dokumen Pendukung</h3>
                                    </div>
                                </div>
                                <div class="row mb-3">
                                    <div class="col-sm-9">
                                        <h6>KTP</h6>
                                    </div>
                                    <div class="col-sm-3">
                                        <a href="/storage/{{$pelanggan->dokumen}}" data-lightbox="mygallery"><img src="/storage/{{$pelanggan->dokumen}}" height="50" width="80"></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>