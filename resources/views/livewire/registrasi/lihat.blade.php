<div class="content-body">
    <div class="container-fluid">
        <div class="row gutters-sm">
            <div class="col-md-4">
                <div class="card">
                    <div class="card-body">
                        <div class="d-flex flex-column align-items-center text-center">
                            <img src="/storage/{{$foto}}" alt="Admin" width="300" height="350">
                            <div class="mt-3">
                                <h4>{{$uname}}</h4>
                            </div>
                        </div>
                        <div class="d-flex justify-content-between mx-5 mt-5">
                            <button class="btn btn-primary" wire:click="konfirmasi">Verifikasi</button>
                            <button class="btn btn-danger" wire:click="tolak">Tolak</button>
                        </div>
                    </div>
                </div>

            </div>
            <div class="col-md-8">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-sm-3">
                                <h6 class="mb-0">Jenis</h6>
                            </div>
                            <div class="col-sm-9" style="color:black;">
                                {{$jenis}}
                            </div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-sm-3">
                                <h6 class="mb-0">Username</h6>
                            </div>
                            <div class="col-sm-9" style="color:black;">
                                {{$uname}}
                            </div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-sm-3">
                                <h6 class="mb-0">Tanggal Lahir</h6>
                            </div>
                            <div class="col-sm-9" style="color:black;">
                                {{$tanggal_lahir->format('d, F Y')}}
                            </div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-sm-3">
                                <h6 class="mb-0">KTP</h6>
                            </div>
                            <div class="col-sm-9" style="color:black;">
                                <img src="/storage/{{$ktp}}" alt="Admin" width="400" height="300">
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>