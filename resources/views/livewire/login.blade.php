<div>
    <div class="page-links">
        <a href="/" class="active">Login</a><a href="/daftar/pekerja">Daftar Pekerja</a><a href="/daftar/pelanggan">Daftar Pelanggan</a>
    </div>
    @if(session()->get('error'))
    <div class="alert alert-warning alert-dismissible fade show with-icon" role="alert">
        {{ session('error') }}
    </div>
    @endif
    <form wire:submit.prevent="login" autocomplete="off">
        <label for="">Username</label>
        <input class="form-control" type="text" wire:model="username" placeholder="Username" required>
        <label for="">Password</label>
        <input class="form-control" type="password" wire:model="password" placeholder="Password" required>
        <div class="form-button">
            <button id="submit" type="submit" class="ibtn">Login</button>
        </div>
    </form>
</div>